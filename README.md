# DbPraktikum-GR13-2021

>> https://git.informatik.uni-leipzig.de/dbs/dbpraktikum-mediastore

# Client for project

>> https://git.informatik.uni-leipzig.de/ak48ruju/dbpraktikum-gr13-2021-client

### Code conventions

* camelcase **!Always**
* try to keep classes <60 lines if possible
* **NO inline comments** everything you need to know about the function should be
described in its java doc comment above
* as for formatting maybe just go with intellij ```crtl alt shift l```
