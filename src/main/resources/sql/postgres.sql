--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0 (Debian 13.0-1.pgdg100+1)
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE IF EXISTS ONLY mediastore.transaction DROP CONSTRAINT IF EXISTS transaction_user_username_fk;
ALTER TABLE IF EXISTS ONLY mediastore.song DROP CONSTRAINT IF EXISTS song_music_cd_music_cd_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.similar_products DROP CONSTRAINT IF EXISTS similar_products_product_product_id_fk_2;
ALTER TABLE IF EXISTS ONLY mediastore.similar_products DROP CONSTRAINT IF EXISTS similar_products_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.review DROP CONSTRAINT IF EXISTS review_user_fk;
ALTER TABLE IF EXISTS ONLY mediastore.review DROP CONSTRAINT IF EXISTS review_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.publisher_book DROP CONSTRAINT IF EXISTS publisher_book_publisher_publisher_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.publisher_book DROP CONSTRAINT IF EXISTS publisher_book_book_book_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.music_cd DROP CONSTRAINT IF EXISTS music_cd_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.music_cd_label DROP CONSTRAINT IF EXISTS music_cd_label_music_cd_music_cd_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.music_cd_label DROP CONSTRAINT IF EXISTS music_cd_label_label_label_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.involved_in_product DROP CONSTRAINT IF EXISTS involved_in_product_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.involved_in_product DROP CONSTRAINT IF EXISTS involved_in_product_media_production_role_media_production_role;
ALTER TABLE IF EXISTS ONLY mediastore.involved_in_product DROP CONSTRAINT IF EXISTS involved_in_product_media_person_media_person_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.dvd DROP CONSTRAINT IF EXISTS dvd_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.offer DROP CONSTRAINT IF EXISTS condition_shop_fk;
ALTER TABLE IF EXISTS ONLY mediastore.offer DROP CONSTRAINT IF EXISTS condition_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.category_entry DROP CONSTRAINT IF EXISTS category_entry_product_product_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.category_entry DROP CONSTRAINT IF EXISTS category_entry_category_category_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.category DROP CONSTRAINT IF EXISTS category_category_category_id_fk;
ALTER TABLE IF EXISTS ONLY mediastore.cart DROP CONSTRAINT IF EXISTS cart_transaction_fk;
ALTER TABLE IF EXISTS ONLY mediastore.cart DROP CONSTRAINT IF EXISTS cart_condition_fk;
ALTER TABLE IF EXISTS ONLY mediastore.book DROP CONSTRAINT IF EXISTS book_product_product_id_fk;
DROP TRIGGER IF EXISTS assign_product_rating_after_update ON mediastore.review;
DROP TRIGGER IF EXISTS assign_product_rating_after_insert ON mediastore.review;
CREATE OR REPLACE VIEW mediastore.view_6 AS
SELECT
    NULL::bigint AS count;
CREATE OR REPLACE VIEW mediastore.view_5 AS
SELECT
    NULL::character varying(50) AS asin,
    NULL::character varying(150) AS title;
DROP INDEX IF EXISTS mediastore.similar_products_similar_products_id_uindex;
DROP INDEX IF EXISTS mediastore.publisher_name_uindex;
DROP INDEX IF EXISTS mediastore.product_ean_uindex;
DROP INDEX IF EXISTS mediastore.product_asin_uindex;
DROP INDEX IF EXISTS mediastore.music_cd_music_cd_id_uindex;
DROP INDEX IF EXISTS mediastore.label_name_uindex;
ALTER TABLE IF EXISTS ONLY mediastore."user" DROP CONSTRAINT IF EXISTS user_pk;
ALTER TABLE IF EXISTS ONLY mediastore.involved_in_product DROP CONSTRAINT IF EXISTS unique_product_id_media_production_role_id_media_person_id;
ALTER TABLE IF EXISTS ONLY mediastore.transaction DROP CONSTRAINT IF EXISTS transaction_pk;
ALTER TABLE IF EXISTS ONLY mediastore.song DROP CONSTRAINT IF EXISTS song_pk;
ALTER TABLE IF EXISTS ONLY mediastore.similar_products DROP CONSTRAINT IF EXISTS similar_products_unique_constraint;
ALTER TABLE IF EXISTS ONLY mediastore.similar_products DROP CONSTRAINT IF EXISTS similar_products_pk;
ALTER TABLE IF EXISTS ONLY mediastore.shop DROP CONSTRAINT IF EXISTS shop_unique_constraint;
ALTER TABLE IF EXISTS ONLY mediastore.shop DROP CONSTRAINT IF EXISTS shop_pk;
ALTER TABLE IF EXISTS ONLY mediastore.review DROP CONSTRAINT IF EXISTS review_pk;
ALTER TABLE IF EXISTS ONLY mediastore.publisher DROP CONSTRAINT IF EXISTS publisher_pk;
ALTER TABLE IF EXISTS ONLY mediastore.publisher_book DROP CONSTRAINT IF EXISTS publisher_book_pk;
ALTER TABLE IF EXISTS ONLY mediastore.product DROP CONSTRAINT IF EXISTS product_pk;
ALTER TABLE IF EXISTS ONLY mediastore.music_cd DROP CONSTRAINT IF EXISTS music_cd_pk;
ALTER TABLE IF EXISTS ONLY mediastore.music_cd_label DROP CONSTRAINT IF EXISTS music_cd_label_pk;
ALTER TABLE IF EXISTS ONLY mediastore.media_production_role DROP CONSTRAINT IF EXISTS media_production_role_pk;
ALTER TABLE IF EXISTS ONLY mediastore.media_person DROP CONSTRAINT IF EXISTS media_person_pk;
ALTER TABLE IF EXISTS ONLY mediastore.label DROP CONSTRAINT IF EXISTS label_pk;
ALTER TABLE IF EXISTS ONLY mediastore.involved_in_product DROP CONSTRAINT IF EXISTS involved_in_product_pk;
ALTER TABLE IF EXISTS ONLY mediastore.dvd DROP CONSTRAINT IF EXISTS dvd_pk;
ALTER TABLE IF EXISTS ONLY mediastore.offer DROP CONSTRAINT IF EXISTS condition_unique_offer;
ALTER TABLE IF EXISTS ONLY mediastore.offer DROP CONSTRAINT IF EXISTS condition_pk;
ALTER TABLE IF EXISTS ONLY mediastore.category DROP CONSTRAINT IF EXISTS category_pk;
ALTER TABLE IF EXISTS ONLY mediastore.category_entry DROP CONSTRAINT IF EXISTS category_entry_pk;
ALTER TABLE IF EXISTS ONLY mediastore.cart DROP CONSTRAINT IF EXISTS cart_pk;
ALTER TABLE IF EXISTS ONLY mediastore.book DROP CONSTRAINT IF EXISTS book_pk;
ALTER TABLE IF EXISTS mediastore.transaction ALTER COLUMN transaction_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.song ALTER COLUMN song_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.similar_products ALTER COLUMN similar_products_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.shop ALTER COLUMN shop_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.review ALTER COLUMN review_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.publisher ALTER COLUMN publisher_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.product ALTER COLUMN product_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.offer ALTER COLUMN offer_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.media_production_role ALTER COLUMN media_production_role_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.media_person ALTER COLUMN media_person_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.label ALTER COLUMN label_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.involved_in_product ALTER COLUMN involved_in_product_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.category_entry ALTER COLUMN category_entry_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.category ALTER COLUMN category_id DROP DEFAULT;
ALTER TABLE IF EXISTS mediastore.cart ALTER COLUMN cart_id DROP DEFAULT;
DROP VIEW IF EXISTS mediastore.view_9;
DROP VIEW IF EXISTS mediastore.view_8;
DROP VIEW IF EXISTS mediastore.view_7;
DROP VIEW IF EXISTS mediastore.view_6;
DROP VIEW IF EXISTS mediastore.view_5;
DROP VIEW IF EXISTS mediastore.view_4;
DROP VIEW IF EXISTS mediastore.view_3;
DROP VIEW IF EXISTS mediastore.view_2;
DROP VIEW IF EXISTS mediastore.view_12;
DROP VIEW IF EXISTS mediastore.view_11;
DROP VIEW IF EXISTS mediastore.view_10;
DROP VIEW IF EXISTS mediastore.view_1;
DROP TABLE IF EXISTS mediastore."user";
DROP SEQUENCE IF EXISTS mediastore."transaction_transactionId_seq";
DROP TABLE IF EXISTS mediastore.transaction;
DROP SEQUENCE IF EXISTS mediastore.song_song_id_seq;
DROP TABLE IF EXISTS mediastore.song;
DROP SEQUENCE IF EXISTS mediastore.similar_products_similar_products_id_seq;
DROP VIEW IF EXISTS mediastore.similar_product_but_different_main_category;
DROP TABLE IF EXISTS mediastore.similar_products;
DROP SEQUENCE IF EXISTS mediastore."shop_shopId_seq";
DROP TABLE IF EXISTS mediastore.shop;
DROP SEQUENCE IF EXISTS mediastore."review_reviewId_seq";
DROP TABLE IF EXISTS mediastore.review;
DROP SEQUENCE IF EXISTS mediastore.publisher_id_seq;
DROP TABLE IF EXISTS mediastore.publisher_book;
DROP TABLE IF EXISTS mediastore.publisher;
DROP SEQUENCE IF EXISTS mediastore.product_product_id_seq;
DROP TABLE IF EXISTS mediastore.music_cd_label;
DROP TABLE IF EXISTS mediastore.music_cd;
DROP SEQUENCE IF EXISTS mediastore.media_production_role_id_seq;
DROP TABLE IF EXISTS mediastore.media_production_role;
DROP SEQUENCE IF EXISTS mediastore.media_person_id_seq;
DROP TABLE IF EXISTS mediastore.media_person;
DROP VIEW IF EXISTS mediastore.main_categories_of_products;
DROP TABLE IF EXISTS mediastore.product;
DROP SEQUENCE IF EXISTS mediastore.label_label_id_seq;
DROP TABLE IF EXISTS mediastore.label;
DROP SEQUENCE IF EXISTS mediastore.involved_in_product_involved_in_product_id_seq1;
DROP SEQUENCE IF EXISTS mediastore.involved_in_product_involved_in_product_id_seq;
DROP TABLE IF EXISTS mediastore.involved_in_product;
DROP TABLE IF EXISTS mediastore.dvd;
DROP SEQUENCE IF EXISTS mediastore."condition_offerId_seq";
DROP TABLE IF EXISTS mediastore.offer;
DROP SEQUENCE IF EXISTS mediastore.category_entry_category_entry_id_seq;
DROP TABLE IF EXISTS mediastore.category_entry;
DROP SEQUENCE IF EXISTS mediastore.category_category_id_seq;
DROP TABLE IF EXISTS mediastore.category;
DROP SEQUENCE IF EXISTS mediastore."cart_cartId_seq";
DROP TABLE IF EXISTS mediastore.cart;
DROP TABLE IF EXISTS mediastore.book;
DROP PROCEDURE IF EXISTS mediastore.set_product_rating(id bigint);
DROP FUNCTION IF EXISTS mediastore.set_product_rating();
DROP FUNCTION IF EXISTS mediastore.get_product_rating(id bigint);
DROP SCHEMA IF EXISTS mediastore;
--
-- Name: mediastore; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA mediastore;


ALTER SCHEMA mediastore OWNER TO postgres;

--
-- Name: get_product_rating(bigint); Type: FUNCTION; Schema: mediastore; Owner: postgres
--

CREATE FUNCTION mediastore.get_product_rating(id bigint) RETURNS real
    LANGUAGE plpgsql
    AS $$
declare
    average_rating float4;
begin
    select sum(cast(rating AS float4)) / cast(count(*) as float4)
    into average_rating
    from mediastore.review
    where product_id_fk = id;

    return average_rating;
end;
$$;


ALTER FUNCTION mediastore.get_product_rating(id bigint) OWNER TO postgres;

--
-- Name: set_product_rating(); Type: FUNCTION; Schema: mediastore; Owner: postgres
--

CREATE FUNCTION mediastore.set_product_rating() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
    id                  bigint := new.product_id_fk;
    rating_from_reviews float4 := mediastore.get_product_rating(id);
begin

    update mediastore.product
    set rating = rating_from_reviews
    where product_id = id;
    return new;
end;
$$;


ALTER FUNCTION mediastore.set_product_rating() OWNER TO postgres;

--
-- Name: set_product_rating(bigint); Type: PROCEDURE; Schema: mediastore; Owner: postgres
--

CREATE PROCEDURE mediastore.set_product_rating(id bigint)
    LANGUAGE plpgsql
    AS $$
declare
rating_from_reviews float4 := mediastore.get_product_rating(id);
begin

    update mediastore.product
    set rating = rating_from_reviews
    where product_id = id;

end;
$$;


ALTER PROCEDURE mediastore.set_product_rating(id bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: book; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.book (
    book_id bigint NOT NULL,
    number_of_pages integer,
    release_date date,
    isbn character(13) NOT NULL
);


ALTER TABLE mediastore.book OWNER TO postgres;

--
-- Name: cart; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.cart (
    cart_id bigint NOT NULL,
    transaction_id bigint NOT NULL,
    offer_id bigint NOT NULL
);


ALTER TABLE mediastore.cart OWNER TO postgres;

--
-- Name: cart_cartId_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore."cart_cartId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore."cart_cartId_seq" OWNER TO postgres;

--
-- Name: cart_cartId_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore."cart_cartId_seq" OWNED BY mediastore.cart.cart_id;


--
-- Name: category; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.category (
    category_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    parent_category bigint
);


ALTER TABLE mediastore.category OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.category_category_id_seq OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.category_category_id_seq OWNED BY mediastore.category.category_id;


--
-- Name: category_entry; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.category_entry (
    category_entry_id bigint NOT NULL,
    category_id bigint NOT NULL,
    product_id bigint NOT NULL
);


ALTER TABLE mediastore.category_entry OWNER TO postgres;

--
-- Name: category_entry_category_entry_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.category_entry_category_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.category_entry_category_entry_id_seq OWNER TO postgres;

--
-- Name: category_entry_category_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.category_entry_category_entry_id_seq OWNED BY mediastore.category_entry.category_entry_id;


--
-- Name: offer; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.offer (
    offer_id bigint NOT NULL,
    shop_id bigint NOT NULL,
    product_id bigint NOT NULL,
    availability character varying(50) DEFAULT 'not available'::character varying NOT NULL,
    state character varying(50) NOT NULL,
    currency character varying(50) NOT NULL,
    price integer,
    price_multiplier real,
    CONSTRAINT condition_currency_check CHECK (((currency)::text = ANY (ARRAY[(''::character varying)::text, ('EUR'::character varying)::text]))),
    CONSTRAINT condition_price_check CHECK ((price > 0)),
    CONSTRAINT condition_price_multiplier_check CHECK (((price_multiplier > (0)::double precision) AND (price_multiplier < (1)::double precision))),
    CONSTRAINT condition_state_check CHECK (((state)::text = ANY (ARRAY[('new'::character varying)::text, ('second-hand'::character varying)::text])))
);


ALTER TABLE mediastore.offer OWNER TO postgres;

--
-- Name: condition_offerId_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore."condition_offerId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore."condition_offerId_seq" OWNER TO postgres;

--
-- Name: condition_offerId_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore."condition_offerId_seq" OWNED BY mediastore.offer.offer_id;


--
-- Name: dvd; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.dvd (
    dvd_id bigint NOT NULL,
    format character varying(71),
    duration integer,
    region_code integer,
    CONSTRAINT valid_dvd_region_code CHECK (((region_code >= 0) AND (region_code < 6)))
);


ALTER TABLE mediastore.dvd OWNER TO postgres;

--
-- Name: involved_in_product; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.involved_in_product (
    involved_in_product_id bigint NOT NULL,
    product_id bigint,
    media_production_role_id bigint,
    media_person_id bigint
);


ALTER TABLE mediastore.involved_in_product OWNER TO postgres;

--
-- Name: involved_in_product_involved_in_product_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.involved_in_product_involved_in_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.involved_in_product_involved_in_product_id_seq OWNER TO postgres;

--
-- Name: involved_in_product_involved_in_product_id_seq1; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.involved_in_product_involved_in_product_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.involved_in_product_involved_in_product_id_seq1 OWNER TO postgres;

--
-- Name: involved_in_product_involved_in_product_id_seq1; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.involved_in_product_involved_in_product_id_seq1 OWNED BY mediastore.involved_in_product.involved_in_product_id;


--
-- Name: label; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.label (
    label_id bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE mediastore.label OWNER TO postgres;

--
-- Name: label_label_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.label_label_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.label_label_id_seq OWNER TO postgres;

--
-- Name: label_label_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.label_label_id_seq OWNED BY mediastore.label.label_id;


--
-- Name: product; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.product (
    product_id bigint NOT NULL,
    asin character varying(50) NOT NULL,
    ean character varying(13) NOT NULL,
    title character varying(150) NOT NULL,
    sales_rank integer,
    product_group character varying(50) NOT NULL,
    image_link character varying(1000) NOT NULL,
    store_page character varying(1000) NOT NULL,
    rating real,
    CONSTRAINT product_product_group_check CHECK (((product_group)::text = ANY (ARRAY[('DVD'::character varying)::text, ('Book'::character varying)::text, ('Music'::character varying)::text]))),
    CONSTRAINT product_rating_check CHECK (((rating >= (1)::double precision) AND (rating <= (5)::double precision))),
    CONSTRAINT product_sales_rank_check CHECK ((sales_rank > 0))
);


ALTER TABLE mediastore.product OWNER TO postgres;

--
-- Name: main_categories_of_products; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.main_categories_of_products AS
 WITH RECURSIVE category_main_category(product_id, category_id, category_name, parent_category_id) AS (
         SELECT category_entry.product_id,
            category.category_id,
            category.name,
            category.parent_category
           FROM ((mediastore.category
             JOIN mediastore.category_entry USING (category_id))
             JOIN mediastore.product USING (product_id))
        UNION ALL
         SELECT category_main_category_1.product_id,
            category_main_category_1.parent_category_id,
            category.name,
            category.parent_category
           FROM (mediastore.category
             JOIN category_main_category category_main_category_1 ON ((category.category_id = category_main_category_1.parent_category_id)))
        )
 SELECT DISTINCT category_main_category.product_id,
    category_main_category.category_id,
    category_main_category.category_name,
    category_main_category.parent_category_id
   FROM category_main_category
  WHERE (category_main_category.parent_category_id IS NULL);


ALTER TABLE mediastore.main_categories_of_products OWNER TO postgres;

--
-- Name: media_person; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.media_person (
    media_person_id bigint NOT NULL,
    name character varying(89) NOT NULL
);


ALTER TABLE mediastore.media_person OWNER TO postgres;

--
-- Name: media_person_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.media_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.media_person_id_seq OWNER TO postgres;

--
-- Name: media_person_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.media_person_id_seq OWNED BY mediastore.media_person.media_person_id;


--
-- Name: media_production_role; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.media_production_role (
    media_production_role_id bigint NOT NULL,
    name character varying(8) NOT NULL
);


ALTER TABLE mediastore.media_production_role OWNER TO postgres;

--
-- Name: media_production_role_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.media_production_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.media_production_role_id_seq OWNER TO postgres;

--
-- Name: media_production_role_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.media_production_role_id_seq OWNED BY mediastore.media_production_role.media_production_role_id;


--
-- Name: music_cd; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.music_cd (
    music_cd_id bigint NOT NULL,
    release_date date,
    CONSTRAINT valid_music_cd_release_date CHECK (((release_date >= '1970-01-01'::date) AND (release_date < CURRENT_DATE)))
);


ALTER TABLE mediastore.music_cd OWNER TO postgres;

--
-- Name: music_cd_label; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.music_cd_label (
    music_cd_id bigint NOT NULL,
    label_id bigint NOT NULL
);


ALTER TABLE mediastore.music_cd_label OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.product_product_id_seq OWNER TO postgres;

--
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.product_product_id_seq OWNED BY mediastore.product.product_id;


--
-- Name: publisher; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.publisher (
    publisher_id bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE mediastore.publisher OWNER TO postgres;

--
-- Name: publisher_book; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.publisher_book (
    publisher_id bigint NOT NULL,
    book_id bigint NOT NULL
);


ALTER TABLE mediastore.publisher_book OWNER TO postgres;

--
-- Name: publisher_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.publisher_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.publisher_id_seq OWNER TO postgres;

--
-- Name: publisher_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.publisher_id_seq OWNED BY mediastore.publisher.publisher_id;


--
-- Name: review; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.review (
    review_id bigint NOT NULL,
    username_fk character varying(50) DEFAULT 'guest'::character varying,
    product_id_fk bigint NOT NULL,
    rating integer NOT NULL,
    review_date date NOT NULL,
    helpful integer NOT NULL,
    summary character varying(100) NOT NULL,
    content_review text NOT NULL,
    CONSTRAINT review_helpful_check CHECK ((helpful >= 0)),
    CONSTRAINT review_rating_check CHECK (((rating < 6) AND (rating > 0)))
);


ALTER TABLE mediastore.review OWNER TO postgres;

--
-- Name: review_reviewId_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore."review_reviewId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore."review_reviewId_seq" OWNER TO postgres;

--
-- Name: review_reviewId_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore."review_reviewId_seq" OWNED BY mediastore.review.review_id;


--
-- Name: shop; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.shop (
    shop_id bigint NOT NULL,
    name character varying(50) NOT NULL,
    street character varying(50) NOT NULL,
    zip character varying(50) NOT NULL
);


ALTER TABLE mediastore.shop OWNER TO postgres;

--
-- Name: shop_shopId_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore."shop_shopId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore."shop_shopId_seq" OWNER TO postgres;

--
-- Name: shop_shopId_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore."shop_shopId_seq" OWNED BY mediastore.shop.shop_id;


--
-- Name: similar_products; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.similar_products (
    similar_products_id bigint NOT NULL,
    product_id_1_fk bigint NOT NULL,
    product_id_2_fk bigint NOT NULL
);


ALTER TABLE mediastore.similar_products OWNER TO postgres;

--
-- Name: similar_product_but_different_main_category; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.similar_product_but_different_main_category AS
 SELECT p1.asin AS asin_p1,
    p1.title AS title_p1,
    p2.asin AS asin_p2,
    p2.title AS title_p2
   FROM ((mediastore.similar_products
     JOIN mediastore.product p1 ON ((p1.product_id = similar_products.product_id_1_fk)))
     JOIN mediastore.product p2 ON ((p2.product_id = similar_products.product_id_2_fk)))
  WHERE (( SELECT count(*) AS count
           FROM ((
                         SELECT main_categories_of_products.category_name
                           FROM mediastore.main_categories_of_products
                          WHERE (similar_products.product_id_1_fk = main_categories_of_products.product_id)
                        EXCEPT
                         SELECT main_categories_of_products.category_name
                           FROM mediastore.main_categories_of_products
                          WHERE (similar_products.product_id_2_fk = main_categories_of_products.product_id)
                ) UNION (
                         SELECT main_categories_of_products.category_name
                           FROM mediastore.main_categories_of_products
                          WHERE (similar_products.product_id_2_fk = main_categories_of_products.product_id)
                        EXCEPT
                         SELECT main_categories_of_products.category_name
                           FROM mediastore.main_categories_of_products
                          WHERE (similar_products.product_id_1_fk = main_categories_of_products.product_id)
                )) category_difference) >= 1);


ALTER TABLE mediastore.similar_product_but_different_main_category OWNER TO postgres;

--
-- Name: similar_products_similar_products_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.similar_products_similar_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.similar_products_similar_products_id_seq OWNER TO postgres;

--
-- Name: similar_products_similar_products_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.similar_products_similar_products_id_seq OWNED BY mediastore.similar_products.similar_products_id;


--
-- Name: song; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.song (
    song_id bigint NOT NULL,
    number integer,
    name character varying(130),
    music_cd_id bigint NOT NULL
);


ALTER TABLE mediastore.song OWNER TO postgres;

--
-- Name: song_song_id_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore.song_song_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore.song_song_id_seq OWNER TO postgres;

--
-- Name: song_song_id_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore.song_song_id_seq OWNED BY mediastore.song.song_id;


--
-- Name: transaction; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore.transaction (
    transaction_id bigint NOT NULL,
    username character varying(50) NOT NULL,
    street character varying(50) NOT NULL,
    zip character varying(50) NOT NULL,
    bank_account_number character varying(50) NOT NULL,
    addressee character varying(50) NOT NULL
);


ALTER TABLE mediastore.transaction OWNER TO postgres;

--
-- Name: transaction_transactionId_seq; Type: SEQUENCE; Schema: mediastore; Owner: postgres
--

CREATE SEQUENCE mediastore."transaction_transactionId_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mediastore."transaction_transactionId_seq" OWNER TO postgres;

--
-- Name: transaction_transactionId_seq; Type: SEQUENCE OWNED BY; Schema: mediastore; Owner: postgres
--

ALTER SEQUENCE mediastore."transaction_transactionId_seq" OWNED BY mediastore.transaction.transaction_id;


--
-- Name: user; Type: TABLE; Schema: mediastore; Owner: postgres
--

CREATE TABLE mediastore."user" (
    username character varying(50) NOT NULL
);


ALTER TABLE mediastore."user" OWNER TO postgres;

--
-- Name: view_1; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_1 AS
 SELECT ( SELECT count(*) AS count
           FROM mediastore.book) AS book_count,
    ( SELECT count(*) AS count
           FROM mediastore.music_cd) AS music_cd_count,
    ( SELECT count(*) AS count
           FROM mediastore.dvd) AS dvd_count;


ALTER TABLE mediastore.view_1 OWNER TO postgres;

--
-- Name: view_10; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_10 AS
 SELECT DISTINCT similar_product_but_different_main_category.asin_p1,
    similar_product_but_different_main_category.title_p1
   FROM mediastore.similar_product_but_different_main_category
UNION
 SELECT DISTINCT similar_product_but_different_main_category.asin_p2 AS asin_p1,
    similar_product_but_different_main_category.title_p2 AS title_p1
   FROM mediastore.similar_product_but_different_main_category;


ALTER TABLE mediastore.view_10 OWNER TO postgres;

--
-- Name: view_11; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_11 AS
 SELECT product.asin,
    product.title
   FROM (( SELECT offer.product_id,
            offer.shop_id
           FROM mediastore.offer
          WHERE ((offer.availability)::text = 'available'::text)
          GROUP BY offer.product_id, offer.shop_id) single_offer_per_shop
     JOIN mediastore.product USING (product_id))
  GROUP BY product.asin, product.title
 HAVING (sum(single_offer_per_shop.shop_id) = ( SELECT sum(shop.shop_id) AS sum
           FROM mediastore.shop));


ALTER TABLE mediastore.view_11 OWNER TO postgres;

--
-- Name: view_12; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_12 AS
 SELECT (((( SELECT count(*) AS count
           FROM ((mediastore.offer offer_leipzig
             JOIN mediastore.shop ON ((shop.shop_id = offer_leipzig.shop_id)))
             JOIN mediastore.offer offer_non_leipzig ON ((offer_leipzig.product_id = offer_non_leipzig.product_id)))
          WHERE ((offer_leipzig.product_id IN ( SELECT product.product_id
                   FROM (mediastore.view_11
                     JOIN mediastore.product USING (asin, title)))) AND (offer_leipzig.offer_id > offer_non_leipzig.offer_id) AND (offer_leipzig.price < offer_non_leipzig.price))))::numeric / (( SELECT count(*) AS count
           FROM ((mediastore.offer offer_leipzig
             JOIN mediastore.shop ON ((shop.shop_id = offer_leipzig.shop_id)))
             JOIN mediastore.offer offer_non_leipzig ON ((offer_leipzig.product_id = offer_non_leipzig.product_id)))
          WHERE ((offer_leipzig.product_id IN ( SELECT product.product_id
                   FROM (mediastore.view_11
                     JOIN mediastore.product USING (asin, title)))) AND (offer_leipzig.offer_id > offer_non_leipzig.offer_id))))::numeric) * (100)::numeric) AS percentage;


ALTER TABLE mediastore.view_12 OWNER TO postgres;

--
-- Name: view_2; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_2 AS
( SELECT product.rating,
    product.product_group,
    product.asin
   FROM (mediastore.product
     JOIN mediastore.review ON ((review.product_id_fk = product.product_id)))
  WHERE ((product.product_group)::text = 'Book'::text)
  GROUP BY product.asin, product.product_group, product.rating
  ORDER BY product.rating DESC
 LIMIT 5)
UNION
( SELECT product.rating,
    product.product_group,
    product.asin
   FROM (mediastore.product
     JOIN mediastore.review ON ((review.product_id_fk = product.product_id)))
  WHERE ((product.product_group)::text = 'Music'::text)
  GROUP BY product.asin, product.product_group, product.rating
  ORDER BY (avg(review.rating)) DESC
 LIMIT 5)
UNION
( SELECT product.rating,
    product.product_group,
    product.asin
   FROM (mediastore.product
     JOIN mediastore.review ON ((review.product_id_fk = product.product_id)))
  WHERE ((product.product_group)::text = 'DVD'::text)
  GROUP BY product.asin, product.product_group, product.rating
  ORDER BY (avg(review.rating)) DESC
 LIMIT 5);


ALTER TABLE mediastore.view_2 OWNER TO postgres;

--
-- Name: view_3; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_3 AS
 SELECT DISTINCT product.asin,
    product.title
   FROM (mediastore.product
     JOIN mediastore.offer ON ((offer.product_id = product.product_id)))
  WHERE ((offer.availability)::text = 'not available'::text);


ALTER TABLE mediastore.view_3 OWNER TO postgres;

--
-- Name: view_4; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_4 AS
 SELECT product.asin,
    product.title
   FROM ((mediastore.product
     LEFT JOIN mediastore.offer offer1 ON ((product.product_id = offer1.product_id)))
     LEFT JOIN mediastore.offer offer2 ON ((product.product_id = offer2.product_id)))
  WHERE ((product.product_id IN ( SELECT offer.product_id
           FROM mediastore.offer
          WHERE ((offer.availability)::text = 'available'::text)
          GROUP BY offer.product_id
         HAVING (count(offer.product_id) = 2))) AND (offer1.offer_id > offer2.offer_id) AND ((offer1.price > (2 * offer2.price)) OR (offer2.price > (2 * offer1.price))));


ALTER TABLE mediastore.view_4 OWNER TO postgres;

--
-- Name: view_5; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_5 AS
SELECT
    NULL::character varying(50) AS asin,
    NULL::character varying(150) AS title;


ALTER TABLE mediastore.view_5 OWNER TO postgres;

--
-- Name: view_6; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_6 AS
SELECT
    NULL::bigint AS count;


ALTER TABLE mediastore.view_6 OWNER TO postgres;

--
-- Name: view_7; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_7 AS
 SELECT user_reviewcount_table.username,
    user_reviewcount_table.count_reviews
   FROM ( SELECT review.username_fk AS username,
            count(review.username_fk) AS count_reviews
           FROM (mediastore.review
             JOIN mediastore."user" ON (((review.username_fk)::text = ("user".username)::text)))
          GROUP BY review.username_fk) user_reviewcount_table
  WHERE (((user_reviewcount_table.username)::text <> 'guest'::text) AND (user_reviewcount_table.count_reviews >= 10))
  ORDER BY user_reviewcount_table.count_reviews DESC;


ALTER TABLE mediastore.view_7 OWNER TO postgres;

--
-- Name: view_8; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_8 AS
 SELECT DISTINCT media_person.name
   FROM ((mediastore.involved_in_product involved_as_non_author
     JOIN mediastore.media_person ON ((media_person.media_person_id = involved_as_non_author.media_person_id)))
     LEFT JOIN mediastore.media_production_role ON ((media_production_role.media_production_role_id = involved_as_non_author.media_production_role_id)))
  WHERE ((media_person.media_person_id IN ( SELECT involved_as_author.media_person_id
           FROM ((mediastore.involved_in_product involved_as_author
             LEFT JOIN mediastore.media_person media_person_1 ON ((media_person_1.media_person_id = involved_as_author.media_person_id)))
             LEFT JOIN mediastore.media_production_role media_production_role_1 ON ((media_production_role_1.media_production_role_id = involved_as_author.media_production_role_id)))
          WHERE ((media_production_role_1.name)::text = 'author'::text))) AND ((media_production_role.name)::text <> 'author'::text))
  ORDER BY media_person.name;


ALTER TABLE mediastore.view_8 OWNER TO postgres;

--
-- Name: view_9; Type: VIEW; Schema: mediastore; Owner: postgres
--

CREATE VIEW mediastore.view_9 AS
 SELECT avg(music_cd_song_count.song_count) AS average_songs_per_music_cd
   FROM ( SELECT music_cd.music_cd_id AS music_id,
            count(music_cd.music_cd_id) AS song_count
           FROM (mediastore.music_cd
             JOIN mediastore.song ON ((music_cd.music_cd_id = song.music_cd_id)))
          GROUP BY music_cd.music_cd_id) music_cd_song_count;


ALTER TABLE mediastore.view_9 OWNER TO postgres;

--
-- Name: cart cart_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.cart ALTER COLUMN cart_id SET DEFAULT nextval('mediastore."cart_cartId_seq"'::regclass);


--
-- Name: category category_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category ALTER COLUMN category_id SET DEFAULT nextval('mediastore.category_category_id_seq'::regclass);


--
-- Name: category_entry category_entry_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category_entry ALTER COLUMN category_entry_id SET DEFAULT nextval('mediastore.category_entry_category_entry_id_seq'::regclass);


--
-- Name: involved_in_product involved_in_product_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product ALTER COLUMN involved_in_product_id SET DEFAULT nextval('mediastore.involved_in_product_involved_in_product_id_seq1'::regclass);


--
-- Name: label label_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.label ALTER COLUMN label_id SET DEFAULT nextval('mediastore.label_label_id_seq'::regclass);


--
-- Name: media_person media_person_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.media_person ALTER COLUMN media_person_id SET DEFAULT nextval('mediastore.media_person_id_seq'::regclass);


--
-- Name: media_production_role media_production_role_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.media_production_role ALTER COLUMN media_production_role_id SET DEFAULT nextval('mediastore.media_production_role_id_seq'::regclass);


--
-- Name: offer offer_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.offer ALTER COLUMN offer_id SET DEFAULT nextval('mediastore."condition_offerId_seq"'::regclass);


--
-- Name: product product_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.product ALTER COLUMN product_id SET DEFAULT nextval('mediastore.product_product_id_seq'::regclass);


--
-- Name: publisher publisher_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.publisher ALTER COLUMN publisher_id SET DEFAULT nextval('mediastore.publisher_id_seq'::regclass);


--
-- Name: review review_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.review ALTER COLUMN review_id SET DEFAULT nextval('mediastore."review_reviewId_seq"'::regclass);


--
-- Name: shop shop_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.shop ALTER COLUMN shop_id SET DEFAULT nextval('mediastore."shop_shopId_seq"'::regclass);


--
-- Name: similar_products similar_products_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.similar_products ALTER COLUMN similar_products_id SET DEFAULT nextval('mediastore.similar_products_similar_products_id_seq'::regclass);


--
-- Name: song song_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.song ALTER COLUMN song_id SET DEFAULT nextval('mediastore.song_song_id_seq'::regclass);


--
-- Name: transaction transaction_id; Type: DEFAULT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.transaction ALTER COLUMN transaction_id SET DEFAULT nextval('mediastore."transaction_transactionId_seq"'::regclass);


--
-- Name: book book_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.book
    ADD CONSTRAINT book_pk PRIMARY KEY (book_id);


--
-- Name: cart cart_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.cart
    ADD CONSTRAINT cart_pk PRIMARY KEY (cart_id);


--
-- Name: category_entry category_entry_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category_entry
    ADD CONSTRAINT category_entry_pk PRIMARY KEY (category_entry_id);


--
-- Name: category category_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category
    ADD CONSTRAINT category_pk PRIMARY KEY (category_id);


--
-- Name: offer condition_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.offer
    ADD CONSTRAINT condition_pk PRIMARY KEY (offer_id);


--
-- Name: offer condition_unique_offer; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.offer
    ADD CONSTRAINT condition_unique_offer UNIQUE (shop_id, product_id, state);


--
-- Name: dvd dvd_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.dvd
    ADD CONSTRAINT dvd_pk PRIMARY KEY (dvd_id);


--
-- Name: involved_in_product involved_in_product_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product
    ADD CONSTRAINT involved_in_product_pk PRIMARY KEY (involved_in_product_id);


--
-- Name: label label_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.label
    ADD CONSTRAINT label_pk PRIMARY KEY (label_id);


--
-- Name: media_person media_person_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.media_person
    ADD CONSTRAINT media_person_pk PRIMARY KEY (media_person_id);


--
-- Name: media_production_role media_production_role_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.media_production_role
    ADD CONSTRAINT media_production_role_pk PRIMARY KEY (media_production_role_id);


--
-- Name: music_cd_label music_cd_label_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.music_cd_label
    ADD CONSTRAINT music_cd_label_pk PRIMARY KEY (music_cd_id, label_id);


--
-- Name: music_cd music_cd_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.music_cd
    ADD CONSTRAINT music_cd_pk PRIMARY KEY (music_cd_id);


--
-- Name: product product_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.product
    ADD CONSTRAINT product_pk PRIMARY KEY (product_id);


--
-- Name: publisher_book publisher_book_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.publisher_book
    ADD CONSTRAINT publisher_book_pk PRIMARY KEY (publisher_id, book_id);


--
-- Name: publisher publisher_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.publisher
    ADD CONSTRAINT publisher_pk PRIMARY KEY (publisher_id);


--
-- Name: review review_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.review
    ADD CONSTRAINT review_pk PRIMARY KEY (review_id);


--
-- Name: shop shop_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.shop
    ADD CONSTRAINT shop_pk PRIMARY KEY (shop_id);


--
-- Name: shop shop_unique_constraint; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.shop
    ADD CONSTRAINT shop_unique_constraint UNIQUE (name, street, zip);


--
-- Name: similar_products similar_products_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.similar_products
    ADD CONSTRAINT similar_products_pk PRIMARY KEY (similar_products_id);


--
-- Name: similar_products similar_products_unique_constraint; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.similar_products
    ADD CONSTRAINT similar_products_unique_constraint UNIQUE (product_id_1_fk, product_id_2_fk);


--
-- Name: song song_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.song
    ADD CONSTRAINT song_pk PRIMARY KEY (song_id);


--
-- Name: transaction transaction_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.transaction
    ADD CONSTRAINT transaction_pk PRIMARY KEY (transaction_id);


--
-- Name: involved_in_product unique_product_id_media_production_role_id_media_person_id; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product
    ADD CONSTRAINT unique_product_id_media_production_role_id_media_person_id UNIQUE (product_id, media_production_role_id, media_person_id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (username);


--
-- Name: label_name_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX label_name_uindex ON mediastore.label USING btree (name);


--
-- Name: music_cd_music_cd_id_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX music_cd_music_cd_id_uindex ON mediastore.music_cd USING btree (music_cd_id);


--
-- Name: product_asin_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX product_asin_uindex ON mediastore.product USING btree (asin);


--
-- Name: product_ean_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX product_ean_uindex ON mediastore.product USING btree (ean);


--
-- Name: publisher_name_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX publisher_name_uindex ON mediastore.publisher USING btree (name);


--
-- Name: similar_products_similar_products_id_uindex; Type: INDEX; Schema: mediastore; Owner: postgres
--

CREATE UNIQUE INDEX similar_products_similar_products_id_uindex ON mediastore.similar_products USING btree (similar_products_id);


--
-- Name: view_5 _RETURN; Type: RULE; Schema: mediastore; Owner: postgres
--

CREATE OR REPLACE VIEW mediastore.view_5 AS
 SELECT product.asin,
    product.title
   FROM (mediastore.review
     JOIN mediastore.product ON ((review.product_id_fk = product.product_id)))
  WHERE (review.rating = 1)
  GROUP BY product.product_id
INTERSECT
 SELECT product.asin,
    product.title
   FROM (mediastore.review
     JOIN mediastore.product ON ((review.product_id_fk = product.product_id)))
  WHERE (review.rating = 5)
  GROUP BY product.product_id;


--
-- Name: view_6 _RETURN; Type: RULE; Schema: mediastore; Owner: postgres
--

CREATE OR REPLACE VIEW mediastore.view_6 AS
 SELECT count(*) AS count
   FROM ( SELECT product.product_id,
            product.asin,
            product.ean,
            product.title,
            product.sales_rank,
            product.product_group,
            product.image_link,
            product.store_page,
            product.rating
           FROM mediastore.product
        EXCEPT
         SELECT product.product_id,
            product.asin,
            product.ean,
            product.title,
            product.sales_rank,
            product.product_group,
            product.image_link,
            product.store_page,
            product.rating
           FROM (mediastore.review
             JOIN mediastore.product ON ((review.product_id_fk = product.product_id)))
          GROUP BY product.product_id) products_with_no_review;


--
-- Name: review assign_product_rating_after_insert; Type: TRIGGER; Schema: mediastore; Owner: postgres
--

CREATE TRIGGER assign_product_rating_after_insert AFTER INSERT ON mediastore.review FOR EACH ROW EXECUTE FUNCTION mediastore.set_product_rating();


--
-- Name: review assign_product_rating_after_update; Type: TRIGGER; Schema: mediastore; Owner: postgres
--

CREATE TRIGGER assign_product_rating_after_update AFTER UPDATE ON mediastore.review FOR EACH ROW WHEN ((old.rating IS DISTINCT FROM new.rating)) EXECUTE FUNCTION mediastore.set_product_rating();


--
-- Name: book book_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.book
    ADD CONSTRAINT book_product_product_id_fk FOREIGN KEY (book_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cart cart_condition_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.cart
    ADD CONSTRAINT cart_condition_fk FOREIGN KEY (offer_id) REFERENCES mediastore.offer(offer_id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: cart cart_transaction_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.cart
    ADD CONSTRAINT cart_transaction_fk FOREIGN KEY (transaction_id) REFERENCES mediastore.transaction(transaction_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: category category_category_category_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category
    ADD CONSTRAINT category_category_category_id_fk FOREIGN KEY (parent_category) REFERENCES mediastore.category(category_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: category_entry category_entry_category_category_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category_entry
    ADD CONSTRAINT category_entry_category_category_id_fk FOREIGN KEY (category_id) REFERENCES mediastore.category(category_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: category_entry category_entry_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.category_entry
    ADD CONSTRAINT category_entry_product_product_id_fk FOREIGN KEY (product_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: offer condition_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.offer
    ADD CONSTRAINT condition_product_product_id_fk FOREIGN KEY (product_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: offer condition_shop_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.offer
    ADD CONSTRAINT condition_shop_fk FOREIGN KEY (shop_id) REFERENCES mediastore.shop(shop_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dvd dvd_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.dvd
    ADD CONSTRAINT dvd_product_product_id_fk FOREIGN KEY (dvd_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: involved_in_product involved_in_product_media_person_media_person_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product
    ADD CONSTRAINT involved_in_product_media_person_media_person_id_fk FOREIGN KEY (media_person_id) REFERENCES mediastore.media_person(media_person_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: involved_in_product involved_in_product_media_production_role_media_production_role; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product
    ADD CONSTRAINT involved_in_product_media_production_role_media_production_role FOREIGN KEY (media_production_role_id) REFERENCES mediastore.media_production_role(media_production_role_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: involved_in_product involved_in_product_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.involved_in_product
    ADD CONSTRAINT involved_in_product_product_product_id_fk FOREIGN KEY (product_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: music_cd_label music_cd_label_label_label_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.music_cd_label
    ADD CONSTRAINT music_cd_label_label_label_id_fk FOREIGN KEY (label_id) REFERENCES mediastore.label(label_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: music_cd_label music_cd_label_music_cd_music_cd_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.music_cd_label
    ADD CONSTRAINT music_cd_label_music_cd_music_cd_id_fk FOREIGN KEY (music_cd_id) REFERENCES mediastore.music_cd(music_cd_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: music_cd music_cd_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.music_cd
    ADD CONSTRAINT music_cd_product_product_id_fk FOREIGN KEY (music_cd_id) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: publisher_book publisher_book_book_book_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.publisher_book
    ADD CONSTRAINT publisher_book_book_book_id_fk FOREIGN KEY (book_id) REFERENCES mediastore.book(book_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: publisher_book publisher_book_publisher_publisher_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.publisher_book
    ADD CONSTRAINT publisher_book_publisher_publisher_id_fk FOREIGN KEY (publisher_id) REFERENCES mediastore.publisher(publisher_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: review review_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.review
    ADD CONSTRAINT review_product_product_id_fk FOREIGN KEY (product_id_fk) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: review review_user_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.review
    ADD CONSTRAINT review_user_fk FOREIGN KEY (username_fk) REFERENCES mediastore."user"(username) ON UPDATE CASCADE ON DELETE SET DEFAULT;


--
-- Name: similar_products similar_products_product_product_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.similar_products
    ADD CONSTRAINT similar_products_product_product_id_fk FOREIGN KEY (product_id_1_fk) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: similar_products similar_products_product_product_id_fk_2; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.similar_products
    ADD CONSTRAINT similar_products_product_product_id_fk_2 FOREIGN KEY (product_id_2_fk) REFERENCES mediastore.product(product_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: song song_music_cd_music_cd_id_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.song
    ADD CONSTRAINT song_music_cd_music_cd_id_fk FOREIGN KEY (music_cd_id) REFERENCES mediastore.music_cd(music_cd_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaction transaction_user_username_fk; Type: FK CONSTRAINT; Schema: mediastore; Owner: postgres
--

ALTER TABLE ONLY mediastore.transaction
    ADD CONSTRAINT transaction_user_username_fk FOREIGN KEY (username) REFERENCES mediastore."user"(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

