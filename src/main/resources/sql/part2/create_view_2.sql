drop view mediastore.view_2;
create view mediastore.view_2 as
(
    SELECT product.rating, product.product_group, product.asin
    FROM mediastore.product
    INNER JOIN mediastore.review ON mediastore.review.product_id_fk = mediastore.product.product_id
    WHERE product.product_group = 'Book'
    GROUP BY product.asin, product.product_group, product.rating
    ORDER BY product.rating DESC
    LIMIT 5
)

UNION

(
    SELECT product.rating, product.product_group, product.asin
    FROM mediastore.product
    INNER JOIN mediastore.review ON mediastore.review.product_id_fk = mediastore.product.product_id
    WHERE product.product_group = 'Music'
    GROUP BY product.asin, product.product_group, product.rating
    ORDER BY AVG(review.rating) DESC
    LIMIT 5
)

UNION

(
    SELECT product.rating, product.product_group, product.asin
    FROM mediastore.product
    INNER JOIN mediastore.review ON mediastore.review.product_id_fk = mediastore.product.product_id
    WHERE product.product_group = 'DVD'
    GROUP BY product.asin, product.product_group, product.rating
    ORDER BY AVG(review.rating) DESC
    LIMIT 5
)
;
