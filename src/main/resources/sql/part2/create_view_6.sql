drop view mediastore.view_6;
create view mediastore.view_6 as
-- construct a table with product_ids that have no reviews and join those with the product table
select count(*)
from (
         select *
         from mediastore.product
             except
         select mediastore.product.*
         from mediastore.review
                  join mediastore.product on mediastore.review.product_id_fk = mediastore.product.product_id
         group by mediastore.product.product_id) as products_with_no_review;

