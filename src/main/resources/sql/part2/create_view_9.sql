drop view mediastore.view_9;
create view mediastore.view_9 as
select avg(music_cd_song_count.song_count) as average_songs_per_music_cd
from (select music_cd.music_cd_id as music_id, count(music_cd.music_cd_id) as song_count
      from mediastore.music_cd
               join mediastore.song on mediastore.music_cd.music_cd_id = mediastore.song.music_cd_id
      group by mediastore.music_cd.music_cd_id) as music_cd_song_count;