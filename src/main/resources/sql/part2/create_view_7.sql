drop view mediastore.view_7;
create view mediastore.view_7 as
select *
from (
         select username_fk as username, count(username_fk) as count_reviews
         from mediastore.review
                  inner join mediastore.user on mediastore.review.username_fk = mediastore."user".username
         group by username_fk) as user_reviewcount_table
where username != 'guest'
  and count_reviews >= 10
order by count_reviews desc;