drop view mediastore.view_4;
create view mediastore.view_4 as
SELECT distinct product.asin, product.title
FROM mediastore.product
LEFT JOIN mediastore.offer AS offer1 ON mediastore.product.product_id = offer1.product_id
LEFT JOIN mediastore.offer AS offer2 ON mediastore.product.product_id = offer2.product_id
WHERE product.product_id IN
    (
        SELECT offer.product_id
        FROM mediastore.offer
        GROUP BY offer.product_id
        HAVING count(offer.product_id) > 1
    )
    AND offer1.offer_id > offer2.offer_id
    AND
        (
            offer1.price > 2* offer2.price
            OR
            offer2.price > 2* offer1.price
        )
;
