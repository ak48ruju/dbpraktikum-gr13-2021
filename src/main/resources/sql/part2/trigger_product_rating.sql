create or replace function mediastore.get_product_rating(id bigint)
    returns float4
    language plpgsql
as
$$
declare
    average_rating real;
begin
    select sum(cast(rating AS real)) / cast(count(*) as real)
    into average_rating
    from mediastore.review
    where product_id_fk = id;

    return average_rating;
end;
$$;


create or replace function mediastore.set_product_rating()
    returns trigger
    language plpgsql
as
$$
declare
    id                  bigint := new.product_id_fk;
    rating_from_reviews float4 := mediastore.get_product_rating(id);
begin

    update mediastore.product
    set rating = rating_from_reviews
    where product_id = id;
    return new;
end;
$$;

create trigger assign_product_rating_after_insert
    after insert
    on mediastore.review
    for each row
execute procedure mediastore.set_product_rating();

create trigger assign_product_rating_after_update
    after update
    on mediastore.review
    for each row
    when (old.rating is distinct from new.rating)
execute procedure mediastore.set_product_rating();
