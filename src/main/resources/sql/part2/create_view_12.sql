drop view mediastore.view_12;
create view mediastore.view_12 as
SELECT cast
    (
        (
            SELECT count(*)
            FROM mediastore.offer AS offer_leipzig
            JOIN mediastore.shop on mediastore.shop.shop_id = offer_leipzig.shop_id
            JOIN mediastore.offer AS offer_non_leipzig ON offer_leipzig.product_id = offer_non_leipzig.product_id
            WHERE offer_leipzig.product_id IN
                 (
                     SELECT product_id FROM mediastore.view_11
					 NATURAL JOIN mediastore.product
                 )
                 AND offer_leipzig.offer_id > offer_non_leipzig.offer_id
                 AND offer_leipzig.price < offer_non_leipzig.price
        )
    as decimal
    )
    /
    (
        SELECT count(*)
        FROM mediastore.offer AS offer_leipzig
        JOIN mediastore.shop on mediastore.shop.shop_id = offer_leipzig.shop_id
        JOIN mediastore.offer AS offer_non_leipzig ON offer_leipzig.product_id = offer_non_leipzig.product_id
        WHERE offer_leipzig.product_id IN
             (
                 SELECT product_id
                 FROM mediastore.view_11
				 NATURAL JOIN mediastore.product
             )
             AND offer_leipzig.offer_id > offer_non_leipzig.offer_id
    )
    *100 AS percentage
;

