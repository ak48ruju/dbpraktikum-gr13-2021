drop view mediastore.view_3;
create view mediastore.view_3 as
drop view mediastore.view_3;
create view mediastore.view_3 as
SELECT product.asin, product.title
FROM mediastore.product

EXCEPT

SELECT DISTINCT product.asin, product.title
FROM mediastore.product
         JOIN mediastore.offer ON mediastore.offer.product_id = mediastore.product.product_id
WHERE offer.availability = 'available';
