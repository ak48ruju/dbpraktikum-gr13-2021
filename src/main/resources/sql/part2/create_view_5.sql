drop view mediastore.view_5;
create view mediastore.view_5 as

-- products with at least one 1 star rating
select mediastore.product.asin, mediastore.product.title
from mediastore.review
         join mediastore.product on mediastore.review.product_id_fk = mediastore.product.product_id
where mediastore.review.rating = 1
group by mediastore.product.product_id

intersect

-- products with at least one 5 star rating
select mediastore.product.asin, mediastore.product.title
from mediastore.review
         join mediastore.product on mediastore.review.product_id_fk = mediastore.product.product_id
where mediastore.review.rating = 5
group by mediastore.product.product_id;