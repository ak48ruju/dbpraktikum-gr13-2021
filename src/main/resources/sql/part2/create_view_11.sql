drop view mediastore.view_11;
create view mediastore.view_11 as
SELECT asin, title
FROM
    (
        SELECT  product_id, shop_id
        FROM mediastore.offer
        --WHERE availability = 'available'
        GROUP BY product_id, shop_id
    ) AS single_offer_per_shop
NATURAL JOIN mediastore.product
GROUP BY asin, title
HAVING sum(shop_id) =
    (
        SELECT sum(shop_id)
        FROM mediastore.shop
    );
