drop view mediastore.view_1;
create view mediastore.view_1 as
select (select count(*)
        from mediastore.book)     as book_count,
       (select count(*)
        from mediastore.music_cd) as music_cd_count,
       (select count(*)
        from mediastore.dvd)      as dvd_count;

