drop view mediastore.view_8;
create view mediastore.view_8 as
SELECT DISTINCT media_person.name
FROM mediastore.involved_in_product AS involved_as_non_author
JOIN mediastore.media_person ON mediastore.media_person.media_person_id = involved_as_non_author.media_person_id
LEFT JOIN mediastore.media_production_role ON mediastore.media_production_role.media_production_role_id = involved_as_non_author.media_production_role_id
WHERE media_person.media_person_id IN
    (
        SELECT involved_as_author.media_person_id
        FROM mediastore.involved_in_product AS involved_as_author
        LEFT JOIN mediastore.media_person ON mediastore.media_person.media_person_id = involved_as_author.media_person_id
        LEFT JOIN mediastore.media_production_role ON mediastore.media_production_role.media_production_role_id = involved_as_author.media_production_role_id
        WHERE media_production_role.name = 'author'
    )
    AND media_production_role.name != 'author'
ORDER BY media_person.name ASC;
