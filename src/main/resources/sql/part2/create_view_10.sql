drop view mediastore.main_categories_of_products;
create view mediastore.main_categories_of_products as
with recursive category_main_category(product_id,
                                    asin,
                                      category_id,
                                      category_name,
                                      parent_category_id
    )
                   as
                   (
                       Select product_id,
                              asin,
                              category_id,
                              name,
                              parent_category
                       from mediastore.category
                                natural join mediastore.category_entry
                                natural join mediastore.product
                       union all
                       select category_main_category.product_id,
                              category_main_category.asin,
                              category_main_category.parent_category_id,
                              mediastore.category.name,
                              mediastore.category.parent_category
                       from mediastore.category
                                join category_main_category
                                     on mediastore.category.category_id =
                                        category_main_category.parent_category_id)
select distinct*
from category_main_category
where parent_category_id is null;

drop view mediastore.similar_product_but_different_main_category;

create view mediastore.similar_product_but_different_main_category as
select p1.asin as asin_p1, p1.title as title_p1, p2.asin as asin_p2,p2.title as title_p2
from mediastore.similar_products
    inner join mediastore.product p1 on p1.product_id = similar_products.product_id_1_fk
    inner join mediastore.product p2 on p2.product_id = similar_products.product_id_2_fk
where (
          select count(*)
          from (
                   (
                       (
                           select category_name
                           from mediastore.main_categories_of_products
                           where product_id_1_fk = mediastore.main_categories_of_products.product_id
                       )
                       intersect
                       (
                           select category_name
                           from mediastore.main_categories_of_products
                           where product_id_2_fk = mediastore.main_categories_of_products.product_id
                       )
                   )

               ) as category_difference
      ) < 1
;
drop view mediastore.view_10;

create view mediastore.view_10 as
select distinct asin_p1, title_p1
from mediastore.similar_product_but_different_main_category
union
select distinct asin_p2, title_p2
from mediastore.similar_product_but_different_main_category;
