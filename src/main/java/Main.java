import Root.Server;

public class Main {

    public static void main(String[] args)
    {
        // listen and serve client requests
        Server server = new Server(2000);
        server.run();
    }

}
