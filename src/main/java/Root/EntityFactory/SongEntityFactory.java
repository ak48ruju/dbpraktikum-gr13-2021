package Root.EntityFactory;

import Root.Entities.MusicCdEntity;
import Root.Entities.SongEntity;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class SongEntityFactory extends XmlDocumentMethods {

    public List<SongEntity> generateSongEntitiesByNode(Node item,  MusicCdEntity musicCdEntity) {

        List<SongEntity> songEntityList = new ArrayList<SongEntity>();
        Element tracksNode = findChildElementByName(item, "tracks");

        List<Element> titleNodes = findChildElementsByName(tracksNode, "title");
        for (int trackNumber = 1; trackNumber < titleNodes.size() +1; trackNumber++) {
            Element titleNode = titleNodes.get(trackNumber -1);
            if(titleNode.getTextContent().trim().equals("")) {
                continue;
            }
            SongEntity songEntity = new SongEntity();
            songEntity.setMusicCdEntity(musicCdEntity);
            songEntity.setSongNumber(trackNumber);
            songEntity.setSongName(titleNode.getTextContent().trim());
            songEntityList.add(songEntity);

        }

        return songEntityList;
    }
}
