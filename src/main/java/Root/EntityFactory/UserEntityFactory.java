package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.UserEntity;
import Root.QueryFactory.GetTrolls;
import Root.QueryFactory.GetUserByUserName;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.List;

public class UserEntityFactory extends XmlDocumentMethods{

    /**
     * Generates an UserEntity
     * @param currentItem is a reviews/review node
     * @return generated UserEntity
     */
    public UserEntity generateUserByDocument(Node currentItem){
        UserEntity userEntity = new UserEntity();
        Element user = findChildElementByName(currentItem, "user");
        String userName = user.getTextContent().trim();
        if(userName.equals("")){
            userEntity.setUsername("guest");
        } else {
            userEntity.setUsername(userName);
        }
        return userEntity;
    }

    //public UserEntity generateUser(DatabaseClient databaseClient, String username) {
    public UserEntity generateUserByName(String username) {
        //AddNewUser query = new AddNewUser(userName);
        //List<UserEntity> result = databaseClient.executeQuery(query);
        //return (result.isEmpty())? null : result.get(0);

        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(username);
        //databaseClient.persistEntity(userEntity);
        return userEntity;
    }

    public UserEntity getUserByUserName(DatabaseClient databaseClient, String userName) {
        GetUserByUserName query = new GetUserByUserName(userName);
        List<UserEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty())? null : result.get(0);
    }

    public List<UserEntity> getTrolls(DatabaseClient databaseClient, Double rating, Long minimumAmount)
    {
        GetTrolls query = new GetTrolls(rating, minimumAmount);
        List<UserEntity> result = databaseClient.executeQuery(query);
        return result;

    }

    /**
     * Checks if a certain user is already indexed in the database
     * @param databaseClient
     * @param userName of UserEntity to be looked up
     * @return true if such a user exists, false otherwise
     */
    public boolean userExists(DatabaseClient databaseClient, String userName) {
        GetUserByUserName query = new GetUserByUserName(userName);
        List<UserEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }
}
