package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.OfferEntity;
import Root.Entities.ProductEntity;
import Root.Entities.ShopEntity;
import Root.QueryFactory.GetOffersByAsin;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Used to construct CondtionsEnties
 */
public class OfferEntityFactory extends XmlDocumentMethods {

    public OfferEntityFactory()
    {
    }

    /**
     * Generates a ConditionEntity from a XML Document Node
     * Generates a ConditionEntity from a XML Document Node
     *
     * @param currentItem is a node of /shop/item
     * @param shop        is the corresponding ShopEntity of said node
     * @param product     is the corresponding ShopEntity of said node
     * @return generated ConditionsEntity
     */
    public List<OfferEntity> generateOfferEntitiesByNode(Node currentItem, ShopEntity shop, ProductEntity product)
    {
        OfferEntity offerEntity;
        List<OfferEntity> offerEntities = new ArrayList<>();
        List<Element> priceElements = findChildElementsByName(currentItem, "price");
        for (Element price : priceElements) {
            offerEntity = new OfferEntity();
            setConditionEntityValues(price, offerEntity);
            setOfferEntityAvailability(offerEntity);
            offerEntity.setShopByShopIdFk(shop);
            offerEntity.setProductByProductIdFk(product);
            offerEntities.add(offerEntity);
        }
        return offerEntities;
    }

    private void setOfferEntityAvailability(OfferEntity offerEntity)
    {
        if (Objects.nonNull(offerEntity.getPrice())) {
            offerEntity.setAvailability("available");
        } else {
            offerEntity.setAvailability("not available");
        }
    }

    private void setConditionEntityValues(Element price, OfferEntity offerEntity)
    {
        String currency = price.getAttribute("currency");
        String state = price.getAttribute("state");
        float priceMultiplier = Float.parseFloat(price.getAttribute("mult").trim());

        offerEntity.setCurrency(currency);
        offerEntity.setState(state);
        offerEntity.setPriceMultiplier(priceMultiplier);

        if (Objects.nonNull(price.getFirstChild()) &&
                price.getFirstChild().getNodeType() == Node.TEXT_NODE) {
            Text priceValue = (Text) price.getFirstChild();
            offerEntity.setPrice(Integer.valueOf(priceValue.getData().trim()));
        }
    }

    public List<OfferEntity> getOffersByAsin(DatabaseClient databaseClient, String asin)
    {
        GetOffersByAsin query = new GetOffersByAsin(asin);
        List<OfferEntity> result = databaseClient.executeQuery(query);
        return result;

    }

}
