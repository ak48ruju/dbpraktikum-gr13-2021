package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.CategoryEntity;
import Root.QueryFactory.GetCategoriesByPathPattern;
import Root.QueryFactory.GetCategoryByParentCategory;
import Root.QueryFactory.GetCategoryRootCategories;
import org.w3c.dom.Node;

import java.util.List;

public class CategoryEntityFactory {


    public CategoryEntity generateMainCategoryByNode(Node currentItem) {
        CategoryEntity  categoryEntity= new CategoryEntity();
        categoryEntity.setCategoryName(getCategoryName(currentItem));
        return categoryEntity;
    }

    public CategoryEntity generateSubCategoryByNode(Node currentItem, CategoryEntity parentCategoryEntity){
        CategoryEntity  categoryEntity= new CategoryEntity();
        String name = parentCategoryEntity.getCategoryName() +"/" + getCategoryName(currentItem);
        categoryEntity.setCategoryName(name);
        categoryEntity.setCategoryByParentCategory(parentCategoryEntity);
        return categoryEntity;
    }

    public List<CategoryEntity> getRootCategories(DatabaseClient databaseClient){
        return new GetCategoryRootCategories().execute(databaseClient.getEntityManager());
    }

    public List<CategoryEntity> getChildCategories(DatabaseClient databaseClient, long parentId) {
        return  new GetCategoryByParentCategory(parentId).execute(databaseClient.getEntityManager());
    }

    public List<CategoryEntity> getCategoriesAndSubCategoriesByPath(DatabaseClient databaseClient, String path){
        return new GetCategoriesByPathPattern(path).execute(databaseClient.getEntityManager());
    }


    private String getCategoryName(Node currentItem){
        String text = currentItem.getTextContent();

        return text.split("\n(.*)")[0];
    }
}
