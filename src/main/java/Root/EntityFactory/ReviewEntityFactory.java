package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.ProductEntity;
import Root.Entities.ReviewEntity;
import Root.Entities.UserEntity;
import Root.QueryFactory.GetProductByAsin;
import Root.QueryFactory.GetReviewByUserAndProduct;
import Root.QueryFactory.GetReviewsByAsin;
import Root.QueryFactory.GetUserByUserName;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.Date;
import java.util.List;

public class ReviewEntityFactory {

    private final ProductEntityFactory productEntityFactory = new ProductEntityFactory();

    public ReviewEntityFactory()
    {
    }

    /**
     * Generates a ReviewEntity from an XML DocumentNode
     *
     * @param databaseClient of DatabaseClient
     * @param currentItem    is a /reviews/review node
     * @param userEntityDb   is the corresponding UserEntity of the given node
     * @return generated ReviewEntity
     */
    public ReviewEntity generateReviewEntityByNode(DatabaseClient databaseClient, Node currentItem, UserEntity userEntityDb)
    {
        ReviewEntity reviewEntity = new ReviewEntity();
        NodeList childNodes = currentItem.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node currentChild = childNodes.item(i);
            String name = currentChild.getNodeName();
            String text = currentChild.getTextContent().trim();
            switch (name) {
                case "product":
                    ProductEntity productEntity = productEntityFactory.getProductByAsin(databaseClient, text);
                    reviewEntity.setProductByProductIdFk(productEntity);
                    break;
                case "rating":
                    try {
                        reviewEntity.setRating(Integer.parseInt(text));
                    } catch (NumberFormatException e) {
                        reviewEntity.setRating(-1);
                    }
                    break;
                case "helpful":
                    try {
                        reviewEntity.setHelpful(Integer.parseInt(text));
                    } catch (NumberFormatException e) {
                        reviewEntity.setHelpful(-1);
                    }
                    break;
                case "reviewdate":
                    try {
                        reviewEntity.setReviewDate(Date.valueOf(text));
                    } catch (Exception e) {
                        reviewEntity.setReviewDate(null);
                    }
                    break;
                case "user":
                    reviewEntity.setUserByUsernameFk(userEntityDb);
                    break;
                case "summary":
                    reviewEntity.setSummary(replaceHtmlTags(text));
                    break;
                case "content":
                    reviewEntity.setContentReview(replaceHtmlTags(text));
                    break;
            }
        }

        return reviewEntity;
    }

    public boolean reviewExists(DatabaseClient databaseClient, UserEntity userEntity, ProductEntity productEntity)
    {
        GetReviewByUserAndProduct query = new GetReviewByUserAndProduct(userEntity, productEntity);
        List<ReviewEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    public ReviewEntity getReviewByUserAndProduct(DatabaseClient databaseClient,
                                                  UserEntity userEntity, ProductEntity productEntity)
    {
        GetReviewByUserAndProduct query = new GetReviewByUserAndProduct(userEntity, productEntity);
        List<ReviewEntity> result = databaseClient.executeQuery(query);
        return result.get(0);
    }

    public List<ReviewEntity> getReviewsByAsin(DatabaseClient databaseClient, String asin)
    {
        GetReviewsByAsin query = new GetReviewsByAsin(asin);
        List<ReviewEntity> result = databaseClient.executeQuery(query);
        return result;

    }
    public ReviewEntity generateReviewEntity(
            Integer rating,
            Date date,
            Integer helpful,
            String summary,
            String contentReview,
            UserEntity userEntity,
            ProductEntity productEntity)
    {

        ReviewEntity reviewEntity = new ReviewEntity();
        reviewEntity.setRating(rating);
        reviewEntity.setReviewDate(date);
        reviewEntity.setHelpful(helpful);
        reviewEntity.setSummary(summary);
        reviewEntity.setContentReview(contentReview);
        reviewEntity.setUserByUsernameFk(userEntity);
        reviewEntity.setProductByProductIdFk(productEntity);

        return reviewEntity;
    }

    /**
     * Creates and persists a new review. If the products ASIN is incorrect there is no record inserted.
     * If the username does not exist the user is created in the "user" table.
     *
     * @param databaseClient a database client
     * @param rating rating for a review
     * @param date the date of a review
     * @param helpful the helpful counter, should be zero
     * @param summary the summary of the review
     * @param contentReview the content of the review
     * @param username the reviews username
     * @param asin the products ASIN
     * @return the newly created ReviewEntity
     */
    public ReviewEntity addNewRewiew(
            DatabaseClient databaseClient,
            Integer rating,
            Date date,
            Integer helpful,
            String summary,
            String contentReview,
            String username,
            String asin) throws Exception
    {

        // test if the product exists
        GetProductByAsin queryGetProduct = new GetProductByAsin(asin);
        List<ProductEntity> resultGetProduct = databaseClient.executeQuery(queryGetProduct);
        ProductEntity productEntity;
        if (resultGetProduct.isEmpty()) {
            // return if the product does not exist
            throw new Exception("invalid ASIN");
        } else {
            productEntity = resultGetProduct.get(0);
        }

        // test if the user already exists
        GetUserByUserName queryGetUser = new GetUserByUserName(username);
        List<UserEntity> resultGetUser = databaseClient.executeQuery(queryGetUser);
        UserEntity userEntity;
        if (resultGetUser.isEmpty()) {
            // create the user
            UserEntityFactory userEntityFactory = new UserEntityFactory();
            UserEntity newUserEntity = userEntityFactory.generateUserByName(username);
            databaseClient.persistEntity(newUserEntity);
            // get the new created record
            GetUserByUserName queryGetCreatedUser = new GetUserByUserName(username);
            userEntity = databaseClient.executeQuery(queryGetCreatedUser).get(0);
        } else {
            userEntity = resultGetUser.get(0);
        }

        ReviewEntity newReviewEntity = generateReviewEntity(rating, date, helpful, summary, contentReview, userEntity, productEntity);

        if (databaseClient.persistEntity(newReviewEntity)) {
            return newReviewEntity;
        }
        return null;
    }

    public void updateReviewEntity(ReviewEntity reviewEntityToBeUpdated, ReviewEntity reviewEntityToUpdateWith) {
        reviewEntityToBeUpdated.setContentReview(reviewEntityToUpdateWith.getContentReview());
        reviewEntityToBeUpdated.setRating       (reviewEntityToUpdateWith.getRating());
        reviewEntityToBeUpdated.setHelpful      (reviewEntityToUpdateWith.getHelpful());
        reviewEntityToBeUpdated.setSummary      (reviewEntityToUpdateWith.getSummary());
        reviewEntityToBeUpdated.setReviewDate   (reviewEntityToUpdateWith.getReviewDate());
    }


    private String replaceHtmlTags(String s)
    {
        return s
                .replace("&amp;lt;BR&amp;gt;", " ")
                .replace("&lt;BR&gt;", " ")
                .replace("&amp;lt;P&amp;gt;", " ")
                .replace("&lt;P&gt;;", " ")
                .replace("&amp;quot;", "\"")
                .replace("&quot;", "\"")
                .replace("&amp;amp;#196;", "Ä")
                .replace("&amp;amp;#214;", "Ö")
                .replace("&amp;amp;#220;", "Ü")
                .replace("&amp;amp;#223;", "ß")
                .replace("&amp;amp;#228;", "ä")
                .replace("&amp;amp;#246;", "ö")
                .replace("&amp;amp;#252;", "ü")
                .replace("&amp;amp;#7838;", "ß")
                .replace("&amp;amp;#x27;", "'")
                .replace("&amp;amp;#8222;", "\"")
                .replace("&amp;amp;#8220;", "\"")
                .replace("&amp;amp;#8226;", "*")
                .replace("&amp;amp;#8211;", "-")
                .replace("&amp;amp;#8217;", "'")
                .replace("&amp;amp;#8216;", "'")
                .replace("&amp;amp;#8230;", "~")
                .replace("&amp;amp;#8221;", "\"")
                .replace("&amp;amp;#8364;", "Euro")
                .replace("&amp;amp;#180;", "'")
                .replace("&amp;amp;#176;", "°")
                .replace("&amp;#196;", "Ä")
                .replace("&amp;#214;", "Ö")
                .replace("&amp;#220;", "Ü")
                .replace("&amp;#223;", "ß")
                .replace("&amp;#228;", "ä")
                .replace("&amp;#246;", "ö")
                .replace("&amp;#252;", "ü")
                .replace("&amp;#7838;", "ß")
                .replace("&amp;#x27;", "'")
                .replace("&amp;#8222;", "\"")
                .replace("&amp;#8220;", "\"")
                .replace("&amp;#8226;", "*")
                .replace("&amp;#8211;", "-")
                .replace("&amp;#8217;", "'")
                .replace("&amp;#8216;", "'")
                .replace("&amp;#8230;", "~")
                .replace("&amp;#8221;", "\"")
                .replace("&amp;#8364;", "Euro")
                .replace("&amp;#180;", "'")
                .replace("&amp;#176;", "°")
                .replace("&apos;", "'")
                .replace("&amp;", "&")
                .replace("¿", "\"");

    }

}
