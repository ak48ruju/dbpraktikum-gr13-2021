package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.MediaPersonEntity;
import Root.Entities.ShopEntity;
import Root.QueryFactory.GetMediaPersonByName;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class MediaPersonEntityFactory extends XmlDocumentMethods {

    //public MediaPersonEntity generateMediaPersonEntityFromNode(Node item) {

    //    return null;
    //}

    public boolean mediaPersonExists(DatabaseClient databaseClient, String name)
    {

        GetMediaPersonByName query = new GetMediaPersonByName(name);
        List<MediaPersonEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    public MediaPersonEntity getMediaPersonByName(DatabaseClient databaseClient, String name)
    {

        GetMediaPersonByName query = new GetMediaPersonByName(name);
        List<MediaPersonEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }

    public List<MediaPersonEntity> generateMediaPersonEntities
            (Node item, String mediaPersonsType, String mediaPersonInstanceTypeName, ShopEntity shopEntity)
    {
        List<MediaPersonEntity> mediaPersonEntityList = new ArrayList<>();
        List<String> mediaPersonNames = getMediaPersonsNames(item, mediaPersonsType, mediaPersonInstanceTypeName, shopEntity.getShopName());

        for (String mediaPersonName : mediaPersonNames) {
            MediaPersonEntity mediaPersonEntity = new MediaPersonEntity();
            mediaPersonEntity.setName(mediaPersonName);
            mediaPersonEntityList.add(mediaPersonEntity);

        }
        return mediaPersonEntityList;

    }

    private List<String> getMediaPersonsNames
            (Node item, String mediaPersonsType, String mediaPersonInstanceTypeName,
             String shopName)
    {
        List<String> names = new ArrayList<>();
        Element labelsNode = findChildElementByName(item, mediaPersonsType);

        List<Element> nameNodes = findChildElementsByName(labelsNode, mediaPersonInstanceTypeName);
        for (int j = 0; j < nameNodes.size(); j++) {
            Element labelNode = nameNodes.get(j);

            if (shopName.equals("Leipzig")) {
                names.add(labelNode.getAttribute("name").trim());
            } else if (shopName.equals("Dresden")) {
                names.add(labelNode.getTextContent().trim());
            }
        }
        return names;
    }
}

