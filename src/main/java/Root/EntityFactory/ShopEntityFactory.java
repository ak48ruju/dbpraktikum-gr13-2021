package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.ShopEntity;
import Root.QueryFactory.GetShopsByNameStreetZipQuery;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.List;

/**
 * Class used to construct ShopsEntities
 */
public class ShopEntityFactory {


    public ShopEntityFactory()
    {
    }

    /**
     * Generates a ShopsEntity by Document input
     *
     * @param input xml document - leipzig_transformed_nowhitespace.xml or dresden_nowhitespace.xml
     * @return generate ShopsEntity
     */
    public ShopEntity generateShopEntityByDocument(Document input)
    {
        Element root = input.getDocumentElement();
        ShopEntity entity = new ShopEntity();
        entity.setShopName(root.getAttribute("name").trim());
        entity.setStreet(root.getAttribute("street").trim());
        entity.setZip(root.getAttribute("zip").trim());
        return entity;
    }

    /**
     * Queries Database for a ShopsEntity
     *
     * @param databaseClient entityManager of DatabaseClient
     * @param name           name of shop
     * @param street         street of shop
     * @param zip            zip of shop
     * @return ShopsEntity matching the parameters if found | null otherwise
     */
    public ShopEntity getShopEntityByNameStreetZip(DatabaseClient databaseClient, String name, String street, String zip)
    {
        GetShopsByNameStreetZipQuery query = new GetShopsByNameStreetZipQuery(name, street, zip);
        List<ShopEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }

    public boolean shopExists(DatabaseClient databaseClient, String name, String street, String zip)
    {
        GetShopsByNameStreetZipQuery query = new GetShopsByNameStreetZipQuery(name, street, zip);
        List<ShopEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }


}
