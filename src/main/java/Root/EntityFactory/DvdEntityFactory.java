package Root.EntityFactory;

import Root.Entities.DvdEntity;
import Root.Entities.ProductEntity;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.sql.Date;

public class DvdEntityFactory extends XmlDocumentMethods {

    public DvdEntity generateDvdEntityByNode(Node item, ProductEntity productEntity)
    {

        DvdEntity dvdEntity = new DvdEntity();
        dvdEntity.setDvdId(productEntity.getProductId());
        Node dvdSpec = getDvdSpecFromNode(item);
        dvdEntity.setFormat(getFormatFromNode(dvdSpec));

        Integer duration = getDurationFromNode(dvdSpec);
        if (duration > 0) {
            dvdEntity.setDuration(duration);
        }

        Integer regionCode = getRegionCodeFromNode(dvdSpec);
        if (regionCode >= 0) {
            dvdEntity.setRegionCode(regionCode);
        }

        return dvdEntity;


    }

    private Node getDvdSpecFromNode(Node item)
    {
        return findChildElementByName(item, "dvdspec");
    }

    private String getFormatFromNode(Node dvdspec)
    {
        Element format = findChildElementByName(dvdspec, "format");
        return format.getTextContent().trim();
    }

    private Integer getDurationFromNode(Node dvdspec)
    {
        Element runningtime = findChildElementByName(dvdspec, "runningtime");
        if (runningtime.getTextContent().trim().length() == 0) {
            return -1;
        } else {
            return Integer.parseInt(runningtime.getTextContent().trim());
        }
    }

    private Integer getRegionCodeFromNode(Node dvdspec)
    {
        Element regioncode = findChildElementByName(dvdspec, "regioncode");
        if (regioncode.getTextContent().trim().length() == 0) {
            return -1;
        } else {
            return Integer.parseInt(regioncode.getTextContent().trim());
        }
    }

    // TODO: this value is not represented in the database
    private String getAspectRatioFromNode(Node dvdspec)
    {
        Element aspectratio = findChildElementByName(dvdspec, "aspectratio");
        return aspectratio.getTextContent().trim();
    }

    // TODO: this value is not represented in the database
    private Date getReleaseDateFromNode(Node dvdspec)
    {
        Element releasedate = findChildElementByName(dvdspec, "releasedate");
        try {
            return Date.valueOf(releasedate.getTextContent().trim());
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    // TODO: this value is not represented in the database
    private Date getTheaterReleaseDateFromNode(Node dvdspec)
    {
        Element theatr_release = findChildElementByName(dvdspec, "theatr_release");
        try {
            return Date.valueOf(theatr_release.getTextContent().trim());
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    // TODO: this value is not represented in the database
    private String getUpcFromNode(Node dvdspec, String shopName)
    {
        Element upc = findChildElementByName(dvdspec, "upc");
        if (shopName.equals("Leipzig")) {
            return upc.getAttribute("val").trim();
        } else if (shopName.equals("Dresden")) {
            return upc.getTextContent().trim();
        }
        return null;
    }

}
