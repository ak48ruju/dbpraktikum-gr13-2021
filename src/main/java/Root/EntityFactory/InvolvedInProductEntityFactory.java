package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.InvolvedInProductEntity;
import Root.Entities.MediaPersonEntity;
import Root.Entities.MediaProductionRoleEntity;
import Root.Entities.ProductEntity;
import Root.QueryFactory.GetInvolvedInProductByTriple;

import java.util.List;

public class InvolvedInProductEntityFactory {

    public InvolvedInProductEntity generateInvolvedInProductEntity(ProductEntity productEntity, MediaPersonEntity mediaPersonEntity, MediaProductionRoleEntity mediaProductionRoleEntity) {

        InvolvedInProductEntity involvedInProductEntity = new InvolvedInProductEntity();
        involvedInProductEntity.setProductId(productEntity.getProductId());
        involvedInProductEntity.setMediaProductionRoleId(mediaProductionRoleEntity.getMediaProductionRoleId());
        involvedInProductEntity.setMediaPersonId(mediaPersonEntity.getMediaPersonId());

        return involvedInProductEntity;
    }

    public boolean involvedInProductExists(DatabaseClient databaseClient, long productEntityId, long mediaPersonEntityId, long mediaProductionRoleEntityId) {

        GetInvolvedInProductByTriple query = new GetInvolvedInProductByTriple(productEntityId, mediaPersonEntityId, mediaProductionRoleEntityId);
        List<InvolvedInProductEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

}
