package Root.EntityFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class XmlDocumentMethods {
    /**
     * Searches all child nodes of the given item for the first occurrence of
     * ELEMENT NODE with the name childNodeElementName.
     *
     * @param parent
     * @param childNodeElementName
     * @return first element with the given name if found | null otherwise
     */
    protected Element findChildElementByName(Node parent, String childNodeElementName)
    {
        Node childElement = parent.getFirstChild();
        while (Objects.nonNull(childElement)) {
            if (childElement.getNodeName().equals(childNodeElementName) && childElement.getNodeType() == Node.ELEMENT_NODE) {
                return (Element) childElement;
            }
            childElement = childElement.getNextSibling();

        }
        System.out.println("Could not find nodeElementName");
        return null;
    }

    /**
     * Searches all child nodes of the given item for all occurrences of
     * ELEMENT NODE with the name childNodeElementName.
     *
     * @param parent
     * @param childNodeElementName
     * @return list of all nodes found | null otherwise
     */
    protected List<Element> findChildElementsByName(Node parent, String childNodeElementName)
    {
        ArrayList<Element> elements = new ArrayList<>();
        Node childElement = parent.getFirstChild();
        while (Objects.nonNull(childElement)) {
            if (childElement.getNodeName().equals(childNodeElementName) && childElement.getNodeType() == Node.ELEMENT_NODE) {
                elements.add ((Element) childElement);
            }
            childElement = childElement.getNextSibling();
        }
        return elements;
    }
}
