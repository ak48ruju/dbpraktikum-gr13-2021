package Root.EntityFactory;

import Root.Entities.BookEntity;
import Root.Entities.ProductEntity;
import Root.Entities.PublisherEntity;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.sql.Date;
import java.util.List;

public class BookEntityFactory extends XmlDocumentMethods{

    public BookEntity generateBookEntityByNode(Node item, ProductEntity productEntity, List<PublisherEntity> publisherEntityList)
    {

        BookEntity bookEntity = new BookEntity();
        bookEntity.setBookId(productEntity.getProductId());

        Node bookspec = getBookSpecFromNode(item);

        bookEntity.setIsbn(getIsbnFromNode(bookspec));
        bookEntity.setReleaseDate(getReleaseYearFromNode(bookspec));

        int numberOfPages = getNumberOfPagesFromNode(bookspec);
        if (numberOfPages > 0) {
            bookEntity.setNumberOfPages(numberOfPages);
        }

        bookEntity.setPublisherEntityList(publisherEntityList);

        return bookEntity;
    }

    private Node getBookSpecFromNode(Node item)
    {
        return findChildElementByName(item, "bookspec");
    }

    private String getIsbnFromNode(Node bookspec)
    {
        Element isbn = findChildElementByName(bookspec, "isbn");
        return isbn.getAttribute("val").trim();
    }

    private Date getReleaseYearFromNode(Node bookspec)
    {
        Element publication = findChildElementByName(bookspec, "publication");
        try {
            return Date.valueOf(publication.getAttribute("date").trim());
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    private int getNumberOfPagesFromNode(Node bookspec)
    {
        Element pages = findChildElementByName(bookspec, "pages");
        try {
            return Integer.parseInt(pages.getTextContent().trim());
        } catch (Exception e) {
            System.out.println(e);
        }
        return -1;
    }
}
