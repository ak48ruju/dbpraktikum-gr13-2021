package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.ProductEntity;
import Root.Entities.ShopEntity;
import Root.Entities.SimilarProductsEntity;
import Root.QueryFactory.GetSimilarProducts;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class SimilarProductsEntityFactory extends XmlDocumentMethods{
    private final ProductEntityFactory productEntityFactory = new ProductEntityFactory();

    public List<SimilarProductsEntity> generateSimilarProductsEntitiesByNode(Node currentItem, ShopEntity shopDb, ProductEntity productDb, DatabaseClient databaseClient)
    {
        List<SimilarProductsEntity> productsEntityList = new ArrayList<>();
        List<Element> similarProductElementList = getSimilarProductList(currentItem, shopDb);
        for(Element similarProduct: similarProductElementList){
            String asin = getAsinFromSimilarProduct(shopDb, similarProduct);
            if(productEntityFactory.productExists(databaseClient, asin)) {
                ProductEntity productEntity = productEntityFactory.getProductByAsin(databaseClient, asin);
                productsEntityList.add(generateSimilarProductsEntity(productDb, productEntity));
            }
        }
        return productsEntityList;
    }

    private SimilarProductsEntity generateSimilarProductsEntity(ProductEntity productEntity1, ProductEntity productEntity2) {
        SimilarProductsEntity similarProductsEntity = new SimilarProductsEntity();
        similarProductsEntity.setProductByProductId1Fk(productEntity1);
        similarProductsEntity.setProductByProductId2Fk(productEntity2);
        return similarProductsEntity;
    }

    private String getAsinFromSimilarProduct(ShopEntity shopDb, Element similarProduct)
    {
        String shopName = shopDb.getShopName();
        if(shopName.equals("Leipzig")){
            return findChildElementByName(similarProduct, "asin").getTextContent();
        }else if(shopName.equals("Dresden")){
            return similarProduct.getAttribute("asin");
        }
        return null;
    }

    private List<Element> getSimilarProductList(Node currentItem, ShopEntity shopDb) {
        String shopName = shopDb.getShopName();
        Node similars = findChildElementByName(currentItem, "similars");
        if(shopName.equals("Leipzig")){
            return findChildElementsByName(similars, "sim_product");
        } else if (shopName.equals("Dresden")) {
            return findChildElementsByName(similars, "item");
        }
        return null;
    }

    public List<SimilarProductsEntity> getSimilarProductsEntityById(DatabaseClient databaseClient, ProductEntity productEntity) {
        return new GetSimilarProducts(productEntity).execute(databaseClient.getEntityManager());
    }
}
