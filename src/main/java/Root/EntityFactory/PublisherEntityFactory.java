package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.BookEntity;
import Root.Entities.PublisherEntity;
import Root.QueryFactory.GetPublisherByName;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class PublisherEntityFactory extends XmlDocumentMethods {

    public PublisherEntity getPublisherByName(DatabaseClient databaseClient, String name)
    {

        GetPublisherByName query = new GetPublisherByName(name);
        List<PublisherEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }

    public boolean publisherExists(DatabaseClient databaseClient, String name)
    {

        GetPublisherByName query = new GetPublisherByName(name);
        List<PublisherEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    public List<PublisherEntity> generatePublisherEntitiesByNode (Node item, String shopName, BookEntity bookEntity) {

        List<PublisherEntity> publisherEntityList = new ArrayList<PublisherEntity>();
        List<BookEntity> bookEntityList = new ArrayList<BookEntity>();
        bookEntityList.add(bookEntity);

        List<String> publisherNames = getNamesFromNode(item, shopName);
        for(int i = 0; i < publisherNames.size(); i++) {
            if(publisherNames.get(i).equals("")){
                continue;
            }
            PublisherEntity publisherEntity = new PublisherEntity();
            publisherEntity.setName(publisherNames.get(i));
            publisherEntity.setBookEntityList(bookEntityList);
            publisherEntityList.add(publisherEntity);
        }

        return publisherEntityList;
    }

    // this method only looks for the first publisher and
    // does not handle multiple occurrences of this element

    private List<String> getNamesFromNode(Node item, String shopName) {

        List<String> publisherNames = new ArrayList<String>();
        Element labelsNode = findChildElementByName(item, "publishers");

        List<Element> labelNodes = findChildElementsByName(labelsNode, "publisher");
        for (int j = 0; j < labelNodes.size(); j++) {
            Element labelNode = labelNodes.get(j);

            if (shopName.equals("Leipzig")) {
                Element elementChild2 =  labelNode;
                publisherNames.add(elementChild2.getAttribute("name").trim());
                //return elementChild2.getAttribute("name").trim();

            } else if (shopName.equals("Dresden")) {
                publisherNames.add(labelNode.getTextContent().trim());
                //return labelNode.getTextContent().trim();
            }

        }

        //System.out.println("could not find publisher");
        return publisherNames;
    }

}
