package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.CategoryEntity;
import Root.Entities.CategoryEntryEntity;
import Root.Entities.ProductEntity;
import org.w3c.dom.Node;

import java.util.List;

public class CategoryEntryEntityFactory {
    private final ProductEntityFactory productEntityFactory= new ProductEntityFactory();


    public CategoryEntryEntity generateCategoryEntryEntityByNode(DatabaseClient databaseClient, Node subNode, CategoryEntity mainCategoryEntity)
    {
        CategoryEntryEntity categoryEntryEntity = new CategoryEntryEntity();
        categoryEntryEntity.setCategoryByCategoryIdFk(mainCategoryEntity);
        ProductEntity productEntity = productEntityFactory.getProductByAsin(databaseClient, subNode.getTextContent());
        categoryEntryEntity.setProductByProductIdFk(productEntity);
        return categoryEntryEntity;
    }
}
