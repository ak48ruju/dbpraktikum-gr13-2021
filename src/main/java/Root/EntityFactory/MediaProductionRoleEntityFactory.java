package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.MediaProductionRoleEntity;
import Root.QueryFactory.GetMediaProductionRoleByName;

import java.util.List;

public class MediaProductionRoleEntityFactory {

    public MediaProductionRoleEntity generateFromName(String name)  {
        MediaProductionRoleEntity mediaProductionRoleEntity = new MediaProductionRoleEntity();
        mediaProductionRoleEntity.setName(name);
        return mediaProductionRoleEntity;
    }

    public boolean mediaProductionRoleExists(DatabaseClient databaseClient, String name)
    {

        GetMediaProductionRoleByName query = new GetMediaProductionRoleByName(name);
        List<MediaProductionRoleEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    public MediaProductionRoleEntity getMediaProductionRoleByName(DatabaseClient databaseClient, String name)
    {

        GetMediaProductionRoleByName query = new GetMediaProductionRoleByName(name);
        List<MediaProductionRoleEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }
}
