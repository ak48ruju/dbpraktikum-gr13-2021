package Root.EntityFactory;

import Root.Entities.LabelEntity;
import Root.Entities.MusicCdEntity;
import Root.Entities.ProductEntity;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.sql.Date;
import java.util.List;

public class MusicCdEntityFactory extends XmlDocumentMethods {

    public MusicCdEntity generateMusicCdEntityByNode(Node item, ProductEntity productEntity, List<LabelEntity> labelEntityList)
    {

        MusicCdEntity musicCdEntity = new MusicCdEntity();
        musicCdEntity.setMusicCdId(productEntity.getProductId());
        musicCdEntity.setLabelEntityList(labelEntityList);

        // add music cd specs
        Node musicSpec = getMusicCdSpecFromNode(item);
        musicCdEntity.setReleaseDate(getReleaseDateFromNode(musicSpec));

        return musicCdEntity;
    }

    private Node getMusicCdSpecFromNode(Node item)
    {
        return findChildElementByName(item, "musicspec");
    }

    // TODO: this value is not represented in the database
    private String getBindingFromNode(Node musicspec)
    {
        Element binding = findChildElementByName(musicspec, "binding");
        return binding.getTextContent().trim();
    }

    // TODO: this value is not represented in the database
    private String getFormatFromNode(Node musicspec)
    {
        Element format = findChildElementByName(musicspec, "format");
        return format.getTextContent().trim();
    }

    private Date getReleaseDateFromNode(Node musicspec)
    {
        Element releasedate = findChildElementByName(musicspec, "releasedate");
        try {
            return Date.valueOf(releasedate.getTextContent().trim());
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    // TODO: this value is not represented in the database
    private String getUpcFromNode(Node musicspec)
    {
        Element upc = findChildElementByName(musicspec, "upc");
        return upc.getTextContent().trim();
    }
}
