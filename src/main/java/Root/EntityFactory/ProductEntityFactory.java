package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.ProductEntity;
import Root.QueryFactory.GetProductByAsin;
import Root.QueryFactory.GetProductsByTitlePattern;
import Root.QueryFactory.GetProductsByTopKRank;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.List;
import java.util.Objects;

/**
 * Used to construct ProductEntities
 */

public class ProductEntityFactory extends XmlDocumentMethods {



    public ProductEntityFactory()
    {

    }

    public ProductEntity getProductByAsin(DatabaseClient databaseClient, String asin)
    {

        GetProductByAsin query = new GetProductByAsin(asin);
        List<ProductEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }

    public List<ProductEntity> getProductsByTopKRank(DatabaseClient databaseClient, Integer k)
    {

        GetProductsByTopKRank query = new GetProductsByTopKRank(k);
        List<ProductEntity> result = databaseClient.executeQuery(query);
        return result;
    }

    public List<ProductEntity> getProductsByTitlePattern(DatabaseClient databaseClient, String pattern)
    {

        GetProductsByTitlePattern query = new GetProductsByTitlePattern(pattern);
        List<ProductEntity> result = databaseClient.executeQuery(query);
        return result;
    }

    public boolean productExists(DatabaseClient databaseClient, String asin)
    {

        GetProductByAsin query = new GetProductByAsin(asin);
        List<ProductEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    /**
     * Generates a ProductEntity by Document doc
     *
     * @return generate a ProductEntity
     */

    public ProductEntity generateProductEntityByNode(Node currentItem, String shopName)
    {
        Element itemElement = (Element) currentItem;
        ProductEntity productEntity = new ProductEntity();
        productEntity.setAsin(itemElement.getAttribute("asin"));

        if (!itemElement.getAttribute("salesrank").equals("")) {
            productEntity.setSalesRank(
                    Integer.parseInt(
                            itemElement.getAttribute("salesrank").trim()));
        }
        if (Objects.nonNull(findChildElementByName(itemElement, "title"))) {
            productEntity.setTitle(findChildElementByName(itemElement, "title").getTextContent().trim());
        }

        productEntity.setProductGroup(itemElement.getAttribute("pgroup").trim());
        productEntity.setEan(getEanFromItem(itemElement, shopName));
        productEntity.setImageLink(getImgLinkFromItem(itemElement, shopName));
        productEntity.setStorePage(getStorePageLinkFromItem(itemElement, shopName));
        return productEntity;
    }

    private String getEanFromItem(Element currentItem, String shopName)
    {
        if (shopName.equals("Leipzig")) {
            return currentItem.getAttribute("ean").trim();
        } else if (shopName.equals("Dresden")) {
            return findChildElementByName(currentItem, "ean").getTextContent().trim();
        } else {
            return "unknown shop - can't find ean";
        }
    }

    private String getImgLinkFromItem(Element currentItem, String shopName)
    {
        if (shopName.equals("Leipzig")) {
            return currentItem.getAttribute("picture").trim();
        } else if (shopName.equals("Dresden")) {
            return findChildElementByName(currentItem, "details").getAttribute("img").trim();
        } else {
            return "unknown shop - can't find picture";
        }
    }

    private String getStorePageLinkFromItem(Element currentItem, String shopName)
    {
        if (shopName.equals("Leipzig")) {
            return currentItem.getAttribute("detailpage").trim();
        } else if (shopName.equals("Dresden")) {
            return findChildElementByName(currentItem, "details").getTextContent().trim();
        } else {
            return "unknown shop - can't find store page";
        }
    }

}
