package Root.EntityFactory;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.LabelEntity;
import Root.Entities.MusicCdEntity;
import Root.QueryFactory.GetLabelByName;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

public class LabelEntityFactory extends XmlDocumentMethods{

    public LabelEntity getLabelByName(DatabaseClient databaseClient, String name)
    {

        GetLabelByName query = new GetLabelByName(name);
        List<LabelEntity> result = databaseClient.executeQuery(query);
        return (result.isEmpty()) ? null : result.get(0);
    }

    public boolean labelExists(DatabaseClient databaseClient, String name)
    {

        GetLabelByName query = new GetLabelByName(name);
        List<LabelEntity> result = databaseClient.executeQuery(query);
        return !result.isEmpty();
    }

    public List<LabelEntity> generateLabelsEntitiesByNode (Node item, String shopName, MusicCdEntity musicCdEntity) {

        List<LabelEntity> labelEntityList = new ArrayList<LabelEntity>();
        List <String> labelNames = getNamesFromNode(item, shopName);

        List<MusicCdEntity> musicCdEntityList = new ArrayList<MusicCdEntity>();
        musicCdEntityList.add(musicCdEntity);

        for (int i = 0; i < labelNames.size(); i++) {
            if(labelNames.get(i).equals("")){
                continue;
            }
            LabelEntity labelEntity = new LabelEntity();
            labelEntity.setName(labelNames.get(i));
            labelEntity.setMusicCdEntityList(musicCdEntityList);
            labelEntityList.add(labelEntity);
        }
        return labelEntityList;
    }

    private List<String> getNamesFromNode(Node item, String shopName) {

        List<String> labelList = new ArrayList<String>();
        Element labelsNode = findChildElementByName(item, "labels");

        List<Element> labelNodes = findChildElementsByName(labelsNode, "label");
        for (int j = 0; j < labelNodes.size(); j++) {
            Element labelNode = labelNodes.get(j);

            if (shopName.equals("Leipzig")) {
                labelList.add(labelNode.getAttribute("name").trim());
            } else if (shopName.equals("Dresden")) {
                labelList.add(labelNode.getTextContent().trim());
            }
        }

        return labelList;
    }

}
