package Root.QueryFactory;

import javax.persistence.EntityManager;
import java.util.List;

public interface Queryable<T> {


     List<T> execute(EntityManager entityManager);
}
