package Root.QueryFactory;

import Root.Entities.CategoryEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetCategoryRootCategories implements Queryable<CategoryEntity> {
    @Override
    public List<CategoryEntity> execute(EntityManager entityManager)
    {
        final TypedQuery<CategoryEntity> query = entityManager.createNamedQuery("CategoryEntity.getRootCategories", CategoryEntity.class);
        return query.getResultList();
    }
}
