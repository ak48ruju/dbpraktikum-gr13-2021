package Root.QueryFactory;

import Root.Entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetTrolls implements Queryable<UserEntity> {

    private final Double rating;
    private final Long minimumAmount;

    public GetTrolls(Double rating, Long minimumAmount)
    {
        this.rating = rating;
        this.minimumAmount = minimumAmount;
    }

    @Override
    public List<UserEntity> execute(EntityManager entityManager)
    {
        TypedQuery<UserEntity> query = entityManager.createNamedQuery("user.getTrolls", UserEntity.class);
        query.setParameter("rating",rating);
        query.setParameter("minimumAmount",minimumAmount);
        return query.getResultList();
    }
}
