package Root.QueryFactory;

import Root.Entities.ProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetProductsByTitlePattern implements Queryable<ProductEntity> {

    private final String pattern;


    public GetProductsByTitlePattern(String pattern)
    {
        this.pattern = pattern;

    }

    @Override
    public List<ProductEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ProductEntity> query = entityManager.createNamedQuery("product.byTitlePattern", ProductEntity.class);
        query.setParameter("pattern", "%" + pattern + "%");
        return  query.getResultList();
    }

}
