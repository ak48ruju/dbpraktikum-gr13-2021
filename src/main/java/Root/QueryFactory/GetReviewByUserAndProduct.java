package Root.QueryFactory;

import Root.Entities.ProductEntity;
import Root.Entities.ReviewEntity;
import Root.Entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetReviewByUserAndProduct implements Queryable<ReviewEntity> {

    private final UserEntity userEntity;
    private final ProductEntity productEntity;

    public GetReviewByUserAndProduct(UserEntity userEntity, ProductEntity productEntity)
    {
        this.userEntity = userEntity;
        this.productEntity = productEntity;
    }

    @Override
    public List<ReviewEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ReviewEntity> query = entityManager.createNamedQuery("review.ByUserAndProduct", ReviewEntity.class);
        query.setParameter(1,userEntity);
        query.setParameter(2, productEntity);
        return query.getResultList();
    }
}
