package Root.QueryFactory;

import Root.Entities.MediaPersonEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetMediaPersonByName implements Queryable<MediaPersonEntity> {

    private final String name;


    public GetMediaPersonByName(String name)
    {
        this.name = name;

    }

    @Override
    public List<MediaPersonEntity> execute(EntityManager entityManager)
    {
        TypedQuery<MediaPersonEntity> query = entityManager.createNamedQuery("media_person.byName", MediaPersonEntity.class);
        query.setParameter(1, name);
        return  query.getResultList();
    }

}
