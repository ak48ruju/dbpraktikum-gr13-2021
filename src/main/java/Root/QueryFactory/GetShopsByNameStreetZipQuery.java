package Root.QueryFactory;

import Root.Entities.ShopEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetShopsByNameStreetZipQuery implements Queryable<ShopEntity> {


    private final String name;
    private final String street;
    private final String zip;

    public GetShopsByNameStreetZipQuery(String name, String street, String zip)
    {

        this.name = name;
        this.street = street;
        this.zip = zip;
    }


    @Override
    public List<ShopEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ShopEntity> query = entityManager.createNamedQuery("shops.byNameAndStreetAndZip", ShopEntity.class);
        query.setParameter(1, name);
        query.setParameter(2, street);
        query.setParameter(3, zip);
        return  query.getResultList();
    }
}
