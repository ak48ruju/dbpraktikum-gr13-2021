package Root.QueryFactory;

import Root.Entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetUsersWithAverageRatingBelowK implements Queryable <UserEntity> {

    private final Integer averageRating;

    public GetUsersWithAverageRatingBelowK(Integer averageRating)
    {
        this.averageRating = averageRating;
    }

    @Override
    public List<UserEntity> execute(EntityManager entityManager)
    {
        TypedQuery<UserEntity> query = entityManager.createNamedQuery("user.averageRatingBelowK", UserEntity.class);
        query.setParameter(1,averageRating);
        return query.getResultList();
    }
}
