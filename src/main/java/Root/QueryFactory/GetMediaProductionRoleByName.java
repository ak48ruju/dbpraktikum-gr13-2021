package Root.QueryFactory;

import Root.Entities.MediaProductionRoleEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetMediaProductionRoleByName implements Queryable<MediaProductionRoleEntity> {

    private final String name;


    public GetMediaProductionRoleByName(String name)
    {
        this.name = name;

    }

    @Override
    public List<MediaProductionRoleEntity> execute(EntityManager entityManager)
    {
        TypedQuery<MediaProductionRoleEntity> query = entityManager.createNamedQuery("media_production_role.byName", MediaProductionRoleEntity.class);
        query.setParameter(1, name);
        return  query.getResultList();
    }

}
