package Root.QueryFactory;

import Root.Entities.CategoryEntity;
import Root.Entities.ProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetCategoriesByPathPattern implements Queryable<CategoryEntity> {
    private final String path;

    public GetCategoriesByPathPattern(String path)
    {
        this.path = path;
    }

    @Override
    public List<CategoryEntity> execute(EntityManager entityManager)
    {
        TypedQuery<CategoryEntity> query = entityManager.createNamedQuery("CategoryEntity.byPathAndSubPath", CategoryEntity.class);
        query.setParameter("pattern", path + "%");
        return  query.getResultList();
    }
}
