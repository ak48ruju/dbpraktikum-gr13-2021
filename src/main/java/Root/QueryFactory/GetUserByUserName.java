package Root.QueryFactory;

import Root.Entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetUserByUserName implements Queryable <UserEntity> {

    private final String userName;

    public GetUserByUserName(String userName)
    {
        this.userName = userName;
    }

    @Override
    public List<UserEntity> execute(EntityManager entityManager)
    {
        TypedQuery<UserEntity> query = entityManager.createNamedQuery("user.byUserName", UserEntity.class);
        query.setParameter(1,userName);
        return query.getResultList();
    }
}
