package Root.QueryFactory;

import Root.Entities.OfferEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetOffersByAsin implements Queryable<OfferEntity> {

    private final String asin;


    public GetOffersByAsin(String asin)
    {
        this.asin = asin;

    }

    @Override
    public List<OfferEntity> execute(EntityManager entityManager)
    {
        TypedQuery<OfferEntity> query = entityManager.createNamedQuery("offer.byAsin", OfferEntity.class);
        query.setParameter("asin", asin);
        return  query.getResultList();
    }

}
