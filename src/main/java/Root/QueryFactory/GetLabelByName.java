package Root.QueryFactory;

import Root.Entities.LabelEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetLabelByName implements Queryable<LabelEntity> {

    private final String name;


    public GetLabelByName(String name)
    {
        this.name = name;

    }

    @Override
    public List<LabelEntity> execute(EntityManager entityManager)
    {
        TypedQuery<LabelEntity> query = entityManager.createNamedQuery("label.byName", LabelEntity.class);
        query.setParameter(1, name);
        return  query.getResultList();
    }

}
