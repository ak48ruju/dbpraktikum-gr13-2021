package Root.QueryFactory;

import Root.Entities.PublisherEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetPublisherByName implements Queryable<PublisherEntity> {

    private final String name;


    public GetPublisherByName(String name)
    {
        this.name = name;

    }

    @Override
    public List<PublisherEntity> execute(EntityManager entityManager)
    {
        TypedQuery<PublisherEntity> query = entityManager.createNamedQuery("publisher.byName", PublisherEntity.class);
        query.setParameter(1, name);
        return  query.getResultList();
    }

}
