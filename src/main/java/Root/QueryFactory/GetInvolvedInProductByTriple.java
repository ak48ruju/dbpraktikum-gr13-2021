package Root.QueryFactory;

import Root.Entities.InvolvedInProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetInvolvedInProductByTriple implements Queryable<InvolvedInProductEntity> {

    private final long productEntityId;
    private final long mediaPersonEntityId;
    private final long mediaProductionRoleEntityId;


    public GetInvolvedInProductByTriple(long productEntityId, long mediaPersonEntityId, long mediaProductionRoleEntityId)
    {
        this.productEntityId = productEntityId;
        this.mediaPersonEntityId = mediaPersonEntityId;
        this.mediaProductionRoleEntityId = mediaProductionRoleEntityId;

    }

    @Override
    public List<InvolvedInProductEntity> execute(EntityManager entityManager)
    {
        TypedQuery<InvolvedInProductEntity> query = entityManager.createNamedQuery("involved_in_product.byTriple", InvolvedInProductEntity.class);
        query.setParameter(1, productEntityId);
        query.setParameter(2, mediaPersonEntityId);
        query.setParameter(3, mediaProductionRoleEntityId);
        return  query.getResultList();
    }

}
