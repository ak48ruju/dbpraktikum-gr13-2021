package Root.QueryFactory;

import Root.Entities.ProductEntity;
import Root.Entities.SimilarProductsEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetSimilarProducts implements Queryable<SimilarProductsEntity> {
    private final ProductEntity productEntity;

    public GetSimilarProducts(ProductEntity productEntity)
    {
        this.productEntity = productEntity;
    }

    @Override
    public List<SimilarProductsEntity> execute(EntityManager entityManager)
    {
        TypedQuery<SimilarProductsEntity> query = entityManager.createNamedQuery("similarProducts.getById", SimilarProductsEntity.class);
        query.setParameter(1, productEntity);
        query.setParameter(2,productEntity);
        return query.getResultList();
    }
}
