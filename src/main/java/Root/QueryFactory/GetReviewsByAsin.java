package Root.QueryFactory;

import Root.Entities.ReviewEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetReviewsByAsin implements Queryable<ReviewEntity> {

    private final String asin;

    public GetReviewsByAsin(String asin)
    {
        this.asin = asin;
    }

    @Override
    public List<ReviewEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ReviewEntity> query = entityManager.createNamedQuery("review.ByAsin", ReviewEntity.class);
        query.setParameter("asin",asin);
        return query.getResultList();
    }
}
