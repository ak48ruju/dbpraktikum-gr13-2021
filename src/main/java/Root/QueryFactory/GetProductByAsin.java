package Root.QueryFactory;

import Root.Entities.ProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetProductByAsin implements Queryable<ProductEntity> {

    private final String asin;


    public GetProductByAsin(String asin)
    {
        this.asin = asin;

    }

    @Override
    public List<ProductEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ProductEntity> query = entityManager.createNamedQuery("product.byAsin", ProductEntity.class);
        query.setParameter(1, asin);
        return  query.getResultList();
    }

}
