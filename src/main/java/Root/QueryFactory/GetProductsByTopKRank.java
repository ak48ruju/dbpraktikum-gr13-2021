package Root.QueryFactory;

import Root.Entities.ProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetProductsByTopKRank implements Queryable<ProductEntity> {

    private final Integer rank;


    public GetProductsByTopKRank(Integer rank)
    {
        this.rank = rank;

    }

    @Override
    public List<ProductEntity> execute(EntityManager entityManager)
    {
        TypedQuery<ProductEntity> query = entityManager.createNamedQuery("product.byTopKRank", ProductEntity.class);
        query.setParameter("rank", rank);
        return  query.getResultList();
    }

}

