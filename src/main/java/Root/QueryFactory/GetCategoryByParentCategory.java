package Root.QueryFactory;

import Root.Entities.CategoryEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class GetCategoryByParentCategory implements Queryable<CategoryEntity> {
    public GetCategoryByParentCategory(long parentId)
    {
        this.parentId = parentId;
    }

    private final long parentId;
    @Override
    public List<CategoryEntity> execute(EntityManager entityManager)
    {
        final TypedQuery<CategoryEntity> query = entityManager.createNamedQuery("CategoryEntity.findByParentCategory", CategoryEntity.class);
        query.setParameter(1, parentId);
        return query.getResultList();
    }
}
