package Root.Entities;

import javax.persistence.*;

@NamedQuery(name = "media_production_role.byName",
    query = """
    SELECT mpre
    FROM MediaProductionRoleEntity mpre
    WHERE mpre.name= ?1
    """
)
@Entity
@Table(name = "media_production_role", schema = "mediastore", catalog = "postgres")
public class MediaProductionRoleEntity {
    private long mediaProductionRoleId;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "media_production_role_id")
    public long getMediaProductionRoleId() {
        return mediaProductionRoleId;
    }

    public void setMediaProductionRoleId(long mediaProductionRoleId) {
        this.mediaProductionRoleId = mediaProductionRoleId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MediaProductionRoleEntity that = (MediaProductionRoleEntity) o;

        if (mediaProductionRoleId != that.mediaProductionRoleId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (mediaProductionRoleId ^ (mediaProductionRoleId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
