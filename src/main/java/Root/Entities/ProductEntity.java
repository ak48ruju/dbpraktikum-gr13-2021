package Root.Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedNativeQuery(name = "product.byTopKRank",
    query = """
    SELECT *
    FROM  mediastore.product pe
    WHERE pe.asin IN (
        SELECT pe2.asin
        FROM mediastore.product pe2
        WHERE pe2.asin IN (
            SELECT t.asin
            FROM (
                SELECT pe.asin, DENSE_RANK() OVER (
                    ORDER BY rating DESC
                    ) rating_rank
                FROM mediastore.product pe
                WHERE pe.rating IS NOT NULL
            ) t
            WHERE rating_rank <= :rank
        )
    )
    ORDER BY pe.rating DESC
    """,
    resultClass = ProductEntity.class
)
@NamedQuery(name = "product.byTitlePattern",
    query = """
    SELECT pe
    FROM ProductEntity pe
    WHERE pe.title LIKE :pattern
    """
)
@NamedQuery(name = "product.byAsin",
    query = """
    SELECT pe
    FROM ProductEntity pe
    WHERE pe.asin= ?1
    """
)
@Table(name = "product", schema = "mediastore", catalog = "postgres")
public class ProductEntity {


    private long productId;
    private String asin;
    private String ean;
    private String title;
    private Integer salesRank;
    private String productGroup;
    private String imageLink;
    private String storePage;
    private DvdEntity dvdEntity;
    private BookEntity bookEntity;
    private MusicCdEntity musicCdEntity;
    private transient Collection<CategoryEntryEntity> categoryEntriesByProductId;
    private transient Collection<OfferEntity> offersByProductId;
    private transient Collection<ReviewEntity> reviewsByProductId;
    private Double rating;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", updatable = false, nullable = false)
    public Long getProductId()
    {
        return productId;
    }
    public void setProductId(Long productId)
    {
        this.productId = productId;
    }

    @Basic
    @Column(name = "asin", nullable = false, length = 50)
    public String getAsin()
    {
        return asin;
    }

    public void setAsin(String asin)
    {
        this.asin = asin;
    }

    @Basic
    @Column(name = "ean", nullable = false, length = 13)
    public String getEan()
    {
        return ean;
    }

    public void setEan(String ean)
    {
        this.ean = ean;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 150)
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @Basic
    @Column(name = "sales_rank", nullable = true)
    public Integer getSalesRank()
    {
        return salesRank;
    }

    public void setSalesRank(Integer salesRank)
    {
        this.salesRank = salesRank;
    }

    @Basic
    @Column(name = "product_group", nullable = false, length = 50)
    public String getProductGroup()
    {
        return productGroup;
    }

    public void setProductGroup(String productGroup)
    {
        this.productGroup = productGroup;
    }

    @Basic
    @Column(name = "image_link", nullable = false, length = 1000)
    public String getImageLink()
    {
        return imageLink;
    }

    public void setImageLink(String imageLink)
    {
        this.imageLink = imageLink;
    }

    @Basic
    @Column(name = "store_page", nullable = false, length = 1000)
    public String getStorePage()
    {
        return storePage;
    }

    public void setStorePage(String storePage)
    {
        this.storePage = storePage;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return productId == that.productId &&
                Objects.equals(asin, that.asin) &&
                Objects.equals(ean, that.ean) &&
                Objects.equals(title, that.title) &&
                Objects.equals(salesRank, that.salesRank) &&
                Objects.equals(productGroup, that.productGroup) &&
                Objects.equals(imageLink, that.imageLink) &&
                Objects.equals(rating, that.rating) &&
                Objects.equals(storePage, that.storePage);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(productId, asin, ean, title, salesRank, productGroup, imageLink, storePage, rating);
    }

    @OneToMany(mappedBy = "productByProductIdFk")
    public Collection<CategoryEntryEntity> getCategoryEntriesByProductId()
    {
        return categoryEntriesByProductId;
    }

    public void setCategoryEntriesByProductId(Collection<CategoryEntryEntity> categoryEntriesByProductId)
    {
        this.categoryEntriesByProductId = categoryEntriesByProductId;
    }

    @OneToMany(mappedBy = "productByProductIdFk")
    public Collection<OfferEntity> getOffersByProductId()
    {
        return offersByProductId;
    }

    public void setOffersByProductId(Collection<OfferEntity> offersByProductId)
    {
        this.offersByProductId = offersByProductId;
    }

    @OneToMany(mappedBy = "productByProductIdFk")
    public Collection<ReviewEntity> getReviewsByProductId()
    {
        return reviewsByProductId;
    }

    public void setReviewsByProductId(Collection<ReviewEntity> reviewsByProductId)
    {
        this.reviewsByProductId = reviewsByProductId;
    }


    @OneToOne(mappedBy = "productEntity")
    public MusicCdEntity getMusicCdEntity() {
        return musicCdEntity;
    }

    public void setMusicCdEntity(MusicCdEntity musicCdEntity) {
        this.musicCdEntity = musicCdEntity;
    }

    @OneToOne(mappedBy = "productEntity")
    public BookEntity getBookEntity() {
        return bookEntity;
    }

    public void setBookEntity(BookEntity bookEntity) {
        this.bookEntity = bookEntity;
    }

    @OneToOne(mappedBy = "productEntity")
    public DvdEntity getDvdEntity() {
        return dvdEntity;
    }

    public void setDvdEntity(DvdEntity dvdEntity) {
        this.dvdEntity = dvdEntity;
    }

    @Basic
    @Column(name = "rating", nullable = true)
    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
