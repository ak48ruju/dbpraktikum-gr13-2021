package Root.Entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cart", schema = "mediastore", catalog = "postgres")
public class CartEntity {
    private long cartId;
    private TransactionEntity transactionByTransactionIdFk;
    private OfferEntity offerByOfferIdFk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id", nullable = false)
    public long getCartId()
    {
        return cartId;
    }

    public void setCartId(long cartId)
    {
        this.cartId = cartId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartEntity that = (CartEntity) o;
        return cartId == that.cartId;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(cartId);
    }

    @ManyToOne
    @JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id", nullable = false)
    public TransactionEntity getTransactionByTransactionIdFk()
    {
        return transactionByTransactionIdFk;
    }

    public void setTransactionByTransactionIdFk(TransactionEntity transactionByTransactionIdFk)
    {
        this.transactionByTransactionIdFk = transactionByTransactionIdFk;
    }

    @ManyToOne
    @JoinColumn(name = "offer_id", referencedColumnName = "offer_id", nullable = false)
    public OfferEntity getOfferByOfferIdFk()
    {
        return offerByOfferIdFk;
    }

    public void setOfferByOfferIdFk(OfferEntity offerByOfferIdFk)
    {
        this.offerByOfferIdFk = offerByOfferIdFk;
    }
}
