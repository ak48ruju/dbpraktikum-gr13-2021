package Root.Entities;

import javax.persistence.*;
import java.util.Objects;

@NamedQuery(name = "involved_in_product.byTriple",
    query = """
    SELECT iipe
    FROM InvolvedInProductEntity iipe
    WHERE iipe.productId= ?1
        AND iipe.mediaPersonId= ?2
        AND iipe.mediaProductionRoleId = ?3
"""
)
@Entity
@Table(name = "involved_in_product", schema = "mediastore", catalog = "postgres")
public class InvolvedInProductEntity {
    private long involvedInProductId;
    private long productId;
    private long mediaPersonId;
    private long mediaProductionRoleId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "involved_in_product_id")
    public long getInvolvedInProductId() {
        return involvedInProductId;
    }

    public void setInvolvedInProductId(long involvedInProductId) {
        this.involvedInProductId = involvedInProductId;
    }
    @Basic
    @Column(name = "product_id")
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "media_person_id")
    public long getMediaPersonId() {
        return mediaPersonId;
    }

    public void setMediaPersonId(long mediaPersonId) {
        this.mediaPersonId = mediaPersonId;
    }

    @Basic
    @Column(name = "media_production_role_id")
    public long getMediaProductionRoleId() {
        return mediaProductionRoleId;
    }

    public void setMediaProductionRoleId(long mediaProductionRoleId) {
        this.mediaProductionRoleId = mediaProductionRoleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvolvedInProductEntity that = (InvolvedInProductEntity) o;
        return productId == that.productId && mediaPersonId == that.mediaPersonId && mediaProductionRoleId == that.mediaProductionRoleId && involvedInProductId == that.involvedInProductId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, mediaPersonId, mediaProductionRoleId, involvedInProductId);
    }
}
