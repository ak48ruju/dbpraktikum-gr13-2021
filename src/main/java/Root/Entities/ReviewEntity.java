package Root.Entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@NamedQuery(name = "review.ByUserAndProduct",
    query = """
    SELECT e
    FROM ReviewEntity e
    WHERE e.userByUsernameFk = ?1
        AND e.productByProductIdFk = ?2
    """
)
@NamedQuery(name = "review.ByAsin",
    query = """
    SELECT e
    FROM ReviewEntity e
    INNER JOIN e.productByProductIdFk AS p
    WHERE p.asin = :asin
    """
)

@Table(name = "review", schema = "mediastore", catalog = "postgres")
public class ReviewEntity {
    private long reviewId;
    private int rating;
    private Date reviewDate;
    private int helpful;
    private String summary;
    private String contentReview;
    private UserEntity userByUsernameFk;
    private ProductEntity productByProductIdFk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id", nullable = false)
    public long getReviewId()
    {
        return reviewId;
    }

    public void setReviewId(long reviewId)
    {
        this.reviewId = reviewId;
    }

    @Basic
    @Column(name = "rating", nullable = false)
    public int getRating()
    {
        return rating;
    }

    public void setRating(int rating)
    {
        this.rating = rating;
    }

    @Basic
    @Column(name = "review_date", nullable = false)
    public Date getReviewDate()
    {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate)
    {
        this.reviewDate = reviewDate;
    }

    @Basic
    @Column(name = "helpful", nullable = false)
    public int getHelpful()
    {
        return helpful;
    }

    public void setHelpful(int helpful)
    {
        this.helpful = helpful;
    }

    @Basic
    @Column(name = "summary", nullable = false, length = 100)
    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    @Basic
    @Column(name = "content_review", nullable = false, length = -1)
    public String getContentReview()
    {
        return contentReview;
    }

    public void setContentReview(String contentReview)
    {
        this.contentReview = contentReview;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewEntity that = (ReviewEntity) o;
        return reviewId == that.reviewId &&
                rating == that.rating &&
                helpful == that.helpful &&
                Objects.equals(reviewDate, that.reviewDate) &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(contentReview, that.contentReview);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(reviewId, rating, reviewDate, helpful, summary, contentReview);
    }

    @ManyToOne
    @JoinColumn(name = "username_fk", referencedColumnName = "username")
    public UserEntity getUserByUsernameFk()
    {
        return userByUsernameFk;
    }

    public void setUserByUsernameFk(UserEntity userByUsernameFk)
    {
        this.userByUsernameFk = userByUsernameFk;
    }

    @ManyToOne
    @JoinColumn(name = "product_id_fk", referencedColumnName = "product_id", nullable = false)
    public ProductEntity getProductByProductIdFk()
    {
        return productByProductIdFk;
    }

    public void setProductByProductIdFk(ProductEntity productByProductIdFk)
    {
        this.productByProductIdFk = productByProductIdFk;
    }
}
