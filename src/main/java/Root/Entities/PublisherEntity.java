package Root.Entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQuery(name = "publisher.byName",
    query = """
    SELECT pe
    FROM PublisherEntity pe
    WHERE pe.name= ?1
    """
)
@Table(name = "publisher", schema = "mediastore", catalog = "postgres")
public class PublisherEntity {
    private long publisherId;
    private String name;

    @ManyToMany(mappedBy = "publisherEntityList", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private transient List<BookEntity> bookEntityList;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "publisher_id")
    public long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(long publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    public List<BookEntity> getBookEntityList() {
        return bookEntityList;
    }

    public void setBookEntityList(List<BookEntity> bookEntityList) {
        this.bookEntityList = bookEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublisherEntity that = (PublisherEntity) o;
        return publisherId == that.publisherId && Objects.equals(name, that.name) && Objects.equals(bookEntityList, that.bookEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publisherId, name, bookEntityList);
    }
}
