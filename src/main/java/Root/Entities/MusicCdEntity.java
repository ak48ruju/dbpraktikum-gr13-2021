package Root.Entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.ALL;


@Entity
@Table(name = "music_cd", schema = "mediastore", catalog = "postgres")
public class MusicCdEntity {
    private long musicCdId;
    private Date releaseDate;
    private transient List<LabelEntity> labelEntityList;
    private transient List<SongEntity> songEntityList;
    private transient ProductEntity productEntity;

    @Id
    @Column(name = "music_cd_id")
    public long getMusicCdId() {
        return musicCdId;
    }

    public void setMusicCdId(long musicCdId) {
        this.musicCdId = musicCdId;
    }

    @Basic
    @Column(name = "release_date")
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "mediastore.music_cd_label",
            joinColumns = { @JoinColumn(name = "music_cd_id") },
            inverseJoinColumns = { @JoinColumn(name = "label_id") }
    )
    public List<LabelEntity> getLabelEntityList()
    {
        return labelEntityList;
    }

    public void setLabelEntityList(List<LabelEntity> labelEntityList)
    {
        this.labelEntityList = labelEntityList;
    }

    @OneToMany(mappedBy = "musicCdEntity", cascade = ALL)
    public List<SongEntity> getSongEntityList() {
        return songEntityList;
    }

    public void setSongEntityList(List<SongEntity> songEntityList) {
        this.songEntityList = songEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MusicCdEntity that = (MusicCdEntity) o;
        return musicCdId == that.musicCdId && Objects.equals(releaseDate, that.releaseDate) && Objects.equals(labelEntityList, that.labelEntityList) && Objects.equals(songEntityList, that.songEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(musicCdId, releaseDate, labelEntityList, songEntityList);
    }

    @OneToOne
    @JoinColumn(name = "music_cd_id")
    @MapsId
    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }
}
