package Root.Entities;

import javax.persistence.*;

@Entity
@Table(name = "dvd", schema = "mediastore", catalog = "postgres")
public class DvdEntity {
    private long dvdId;
    private String format;
    private Integer duration;
    private Integer regionCode;
    private transient ProductEntity productEntity;

    @Id
    @Column(name = "dvd_id")
    public long getDvdId() {
        return dvdId;
    }

    public void setDvdId(long dvdId) {
        this.dvdId = dvdId;
    }

    @Basic
    @Column(name = "format")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Basic
    @Column(name = "duration")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "region_code")
    public Integer getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(Integer regionCode) {
        this.regionCode = regionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DvdEntity dvdEntity = (DvdEntity) o;

        if (dvdId != dvdEntity.dvdId) return false;
        if (format != null ? !format.equals(dvdEntity.format) : dvdEntity.format != null) return false;
        if (duration != null ? !duration.equals(dvdEntity.duration) : dvdEntity.duration != null) return false;
        if (regionCode != null ? !regionCode.equals(dvdEntity.regionCode) : dvdEntity.regionCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (dvdId ^ (dvdId >>> 32));
        result = 31 * result + (format != null ? format.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (regionCode != null ? regionCode.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "dvd_id")
    @MapsId
    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }
}
