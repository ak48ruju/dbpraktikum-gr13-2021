package Root.Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQuery(name = "user.averageRatingBelowK",
    query = """
    SELECT ue
    FROM UserEntity ue
    WHERE ue.username IN (
        SELECT re.userByUsernameFk
        FROM ReviewEntity re
        WHERE username_fk != 'guest'
        GROUP BY username_fk
        HAVING avg(rating) < ?1
        )
    """
)
@NamedQuery(name = "user.byUserName",
    query = """
    SELECT ue
    FROM UserEntity ue
    WHERE ue.username= ?1
    """
)
@NamedQuery(name = "user.getTrolls",
    query = """
    SELECT ue
    FROM UserEntity ue
    WHERE ue.username IN (
        SELECT re.userByUsernameFk
        FROM ReviewEntity re
        GROUP BY userByUsernameFk
        HAVING avg(rating) < :rating AND COUNT(*)>= :minimumAmount
    )
    """
)
@Table(name = "user", schema = "mediastore", catalog = "postgres")
public class UserEntity {
    private String username;
    private transient Collection<ReviewEntity> reviewsByUsername;
    private transient Collection<TransactionEntity> transactionsByUsername;

    @Id
    @Column(name = "username", nullable = false, length = 50)
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(username, that.username);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(username);
    }

    @OneToMany(mappedBy = "userByUsernameFk")
    public Collection<ReviewEntity> getReviewsByUsername()
    {
        return reviewsByUsername;
    }

    public void setReviewsByUsername(Collection<ReviewEntity> reviewsByUsername)
    {
        this.reviewsByUsername = reviewsByUsername;
    }

    @OneToMany(mappedBy = "userByUserName")
    public Collection<TransactionEntity> getTransactionsByUsername()
    {
        return transactionsByUsername;
    }

    public void setTransactionsByUsername(Collection<TransactionEntity> transactionsByUsername)
    {
        this.transactionsByUsername = transactionsByUsername;
    }
}
