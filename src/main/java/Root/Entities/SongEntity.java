package Root.Entities;

import javax.persistence.*;

@Entity
@Table(name = "song", schema = "mediastore", catalog = "postgres")
public class SongEntity {
    private long songId;
    private Integer songNumber;
    private String songName;
    private MusicCdEntity musicCdEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "song_id")
    public long getSongId() {
        return songId;
    }

    public void setSongId(long songId) {
        this.songId = songId;
    }

    @Basic
    @Column(name = "number")
    public Integer getSongNumber() {
        return songNumber;
    }

    public void setSongNumber(Integer songNumber) {
        this.songNumber = songNumber;
    }

    @Basic
    @Column(name = "name")
    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SongEntity that = (SongEntity) o;

        if (songId != that.songId) return false;
        if (songNumber != null ? !songNumber.equals(that.songNumber) : that.songNumber != null) return false;
        if (songName != null ? !songName.equals(that.songName) : that.songName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (songId ^ (songId >>> 32));
        result = 31 * result + (songNumber != null ? songNumber.hashCode() : 0);
        result = 31 * result + (songName != null ? songName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "music_cd_id", referencedColumnName = "music_cd_id")
    public MusicCdEntity getMusicCdEntity()
    {
        return musicCdEntity;
    }

    public void setMusicCdEntity(MusicCdEntity musicCdEntity)
    {
        this.musicCdEntity = musicCdEntity;
    }
}
