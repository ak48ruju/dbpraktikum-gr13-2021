package Root.Entities;

import javax.persistence.*;
import java.util.Collection;

@Entity

@NamedNativeQuery(name = "CategoryEntity.findByParentCategory",
        query = """
    SELECT *
    FROM  mediastore.category c
    WHERE c.parent_category = ?1
    """,
        resultClass = CategoryEntity.class
)

@NamedNativeQuery(name = "CategoryEntity.getRootCategories",
        query = """
    SELECT *
    FROM  mediastore.category c
    WHERE c.parent_category is null
    """,
        resultClass = CategoryEntity.class
)
@NamedQuery(name = "CategoryEntity.byPathAndSubPath",
        query = """
    SELECT cat
    FROM CategoryEntity cat
    WHERE cat.categoryName LIKE :pattern
    """
)

@Table(name = "category", schema = "mediastore", catalog = "postgres")
public class CategoryEntity {
    private long categoryId;
    private String categoryName;

    private long parentCategory;

    private CategoryEntity categoryByParentCategory;
    private transient Collection<CategoryEntity> categoriesByCategoryId;
    private transient Collection<CategoryEntryEntity> categoryEntriesByCategoryId;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id", nullable = false)
    public long getCategoryId()
    {
        return categoryId;
    }

    public void setCategoryId(long categoryId)
    {
        this.categoryId = categoryId;
    }


    @Basic
    @Column(name = "name", nullable = false, length = 150)
    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryEntity that = (CategoryEntity) o;

        if (categoryId != that.categoryId) return false;
        if (parentCategory != that.parentCategory) return false;
        if (categoryName != null ? !categoryName.equals(that.categoryName) : that.categoryName != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = (int) (categoryId ^ (categoryId >>> 32));
        result = 31 * result + (categoryName != null ? categoryName.hashCode() : 0);
        result = 31 * result + (int) (parentCategory ^ (parentCategory >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "parent_category", referencedColumnName = "category_id")
    public CategoryEntity getCategoryByParentCategory()
    {
        return categoryByParentCategory;
    }

    public void setCategoryByParentCategory(CategoryEntity categoryByParentCategory)
    {
        this.categoryByParentCategory = categoryByParentCategory;
    }

    @OneToMany(mappedBy = "categoryByParentCategory")
    public Collection<CategoryEntity> getCategoriesByCategoryId()
    {
        return categoriesByCategoryId;
    }

    public void setCategoriesByCategoryId(Collection<CategoryEntity> categoriesByCategoryId)
    {
        this.categoriesByCategoryId = categoriesByCategoryId;
    }

    @OneToMany(mappedBy = "categoryByCategoryIdFk")
    public Collection<CategoryEntryEntity> getCategoryEntriesByCategoryId()
    {
        return categoryEntriesByCategoryId;
    }

    public void setCategoryEntriesByCategoryId(Collection<CategoryEntryEntity> categoryEntriesByCategoryId)
    {
        this.categoryEntriesByCategoryId = categoryEntriesByCategoryId;
    }
}
