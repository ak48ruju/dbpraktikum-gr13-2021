package Root.Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQuery(name = "shops.byNameAndStreetAndZip",
     query = """
     SELECT e
     FROM ShopEntity e
     WHERE e.shopName= ?1
      AND e.street= ?2
      AND e.zip= ?3
     """
)

@Table(name = "shop", schema = "mediastore", catalog = "postgres")
public class ShopEntity {
    private long shopId;
    private String shopName;
    private String street;
    private String zip;
    private transient Collection<OfferEntity> offersByShopId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shop_id", nullable = false)
    public long getShopId()
    {
        return shopId;
    }

    public void setShopId(long shopId)
    {
        this.shopId = shopId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getShopName()
    {
        return shopName;
    }

    public void setShopName(String shopName)
    {
        this.shopName = shopName;
    }

    @Basic
    @Column(name = "street", nullable = false, length = 50)
    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    @Basic
    @Column(name = "zip", nullable = false, length = 50)
    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopEntity that = (ShopEntity) o;
        return shopId == that.shopId &&
                Objects.equals(shopName, that.shopName) &&
                Objects.equals(street, that.street) &&
                Objects.equals(zip, that.zip);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(shopId, shopName, street, zip);
    }

    @OneToMany(mappedBy = "shopByShopIdFk")
    public Collection<OfferEntity> getOffersByShopId()
    {
        return offersByShopId;
    }

    public void setOffersByShopId(Collection<OfferEntity> offersByShopId)
    {
        this.offersByShopId = offersByShopId;
    }
}
