package Root.Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQuery(name = "offer.byAsin",
    query = """
    SELECT o
    FROM OfferEntity o
    INNER JOIN o.productByProductIdFk AS p
    WHERE p.asin = :asin
    """
)

@Table(name = "offer", schema = "mediastore", catalog = "postgres")
public class OfferEntity {
    private long offerId;
    private String availability;
    private String state;
    private String currency;
    private Integer price;
    private Float priceMultiplier;
    private transient Collection<CartEntity> cartsByOfferId;
    private ShopEntity shopByShopIdFk;
    private ProductEntity productByProductIdFk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_id", nullable = false)
    public long getOfferId()
    {
        return offerId;
    }

    public void setOfferId(long offerId)
    {
        this.offerId = offerId;
    }

    @Basic
    @Column(name = "availability", nullable = false, length = 50)
    public String getAvailability()
    {
        return availability;
    }

    public void setAvailability(String availability)
    {
        this.availability = availability;
    }

    @Basic
    @Column(name = "state", nullable = false, length = 50)
    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    @Basic
    @Column(name = "currency", nullable = false, length = 50)
    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public Integer getPrice()
    {
        return price;
    }

    public void setPrice(Integer price)
    {
        this.price = price;
    }

    @Basic
    @Column(name = "price_multiplier", nullable = true, precision = 0)
    public Float getPriceMultiplier()
    {
        return priceMultiplier;
    }

    public void setPriceMultiplier(Float priceMultiplier)
    {
        this.priceMultiplier = priceMultiplier;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Root.Entities.OfferEntity that = (Root.Entities.OfferEntity) o;
        return offerId == that.offerId &&
                Objects.equals(availability, that.availability) &&
                Objects.equals(state, that.state) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(price, that.price) &&
                Objects.equals(priceMultiplier, that.priceMultiplier);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(offerId, availability, state, currency, price, priceMultiplier);
    }

    @OneToMany(mappedBy = "offerByOfferIdFk")
    public Collection<CartEntity> getCartsByOfferId()
    {
        return cartsByOfferId;
    }

    public void setCartsByOfferId(Collection<CartEntity> cartsByOfferId)
    {
        this.cartsByOfferId = cartsByOfferId;
    }

    @ManyToOne
    @JoinColumn(name = "shop_id", referencedColumnName = "shop_id", nullable = false)
    public ShopEntity getShopByShopIdFk()
    {
        return shopByShopIdFk;
    }

    public void setShopByShopIdFk(ShopEntity shopByShopIdFk)
    {
        this.shopByShopIdFk = shopByShopIdFk;
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id", nullable = false)
    public ProductEntity getProductByProductIdFk()
    {
        return productByProductIdFk;
    }

    public void setProductByProductIdFk(ProductEntity productByProductIdFk)
    {
        this.productByProductIdFk = productByProductIdFk;
    }
}
