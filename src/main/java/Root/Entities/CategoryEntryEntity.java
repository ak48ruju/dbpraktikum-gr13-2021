package Root.Entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "category_entry", schema = "mediastore", catalog = "postgres")
public class CategoryEntryEntity {
    private long categoryEntryId;
    private CategoryEntity categoryByCategoryIdFk;
    private ProductEntity productByProductIdFk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_entry_id", nullable = false)
    public long getCategoryEntryId()
    {
        return categoryEntryId;
    }

    public void setCategoryEntryId(long categoryEntryId)
    {
        this.categoryEntryId = categoryEntryId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryEntryEntity that = (CategoryEntryEntity) o;
        return categoryEntryId == that.categoryEntryId;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(categoryEntryId);
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "category_id", nullable = false)
    public CategoryEntity getCategoryByCategoryIdFk()
    {
        return categoryByCategoryIdFk;
    }

    public void setCategoryByCategoryIdFk(CategoryEntity categoryByCategoryIdFk)
    {
        this.categoryByCategoryIdFk = categoryByCategoryIdFk;
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id", nullable = false)
    public ProductEntity getProductByProductIdFk()
    {
        return productByProductIdFk;
    }

    public void setProductByProductIdFk(ProductEntity productByProductIdFk)
    {
        this.productByProductIdFk = productByProductIdFk;
    }
}
