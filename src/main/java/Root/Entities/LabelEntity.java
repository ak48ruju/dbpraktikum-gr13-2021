package Root.Entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@NamedQuery(name = "label.byName",
    query = """
    SELECT le
    FROM LabelEntity le
    WHERE le.name= ?1
    """
)
@Entity
@Table(name = "label", schema = "mediastore", catalog = "postgres")
public class LabelEntity {
    private long labelId;
    private String name;
    @ManyToMany(mappedBy = "labelEntityList", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private transient List<MusicCdEntity> musicCdEntityList;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "label_id")
    public long getLabelId() {
        return labelId;
    }

    public void setLabelId(long labelId) {
        this.labelId = labelId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "labelEntityList")
    public List<MusicCdEntity> getMusicCdEntityList() {
        return musicCdEntityList;
    }

    public void setMusicCdEntityList(List<MusicCdEntity> musicCdEntityList) {
        this.musicCdEntityList = musicCdEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabelEntity that = (LabelEntity) o;
        return labelId == that.labelId && Objects.equals(name, that.name) && Objects.equals(musicCdEntityList, that.musicCdEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(labelId, name, musicCdEntityList);
    }
}
