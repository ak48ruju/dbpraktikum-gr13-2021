package Root.Entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQuery(
        name = "similarProducts.getById",
        query = """
        SELECT sim
        FROM SimilarProductsEntity  sim
        WHERE sim.productByProductId1Fk = ?1 
            OR sim.productByProductId2Fk = ?2
        """
)
@Table(name = "similar_products", schema = "mediastore", catalog = "postgres")
public class SimilarProductsEntity {
    private long similarProductsId;
    private ProductEntity productByProductId1Fk;
    private ProductEntity productByProductId2Fk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "similar_products_id", nullable = false)
    public long getSimilarProductsId()
    {
        return similarProductsId;
    }

    public void setSimilarProductsId(long similarProductsId)
    {
        this.similarProductsId = similarProductsId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimilarProductsEntity that = (SimilarProductsEntity) o;
        return similarProductsId == that.similarProductsId;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(similarProductsId);
    }

    @ManyToOne
    @JoinColumn(name = "product_id_1_fk", referencedColumnName = "product_id", nullable = false)
    public ProductEntity getProductByProductId1Fk()
    {
        return productByProductId1Fk;
    }

    public void setProductByProductId1Fk(ProductEntity productByProductId1Fk)
    {
        this.productByProductId1Fk = productByProductId1Fk;
    }

    @ManyToOne
    @JoinColumn(name = "product_id_2_fk", referencedColumnName = "product_id", nullable = false)
    public ProductEntity getProductByProductId2Fk()
    {
        return productByProductId2Fk;
    }

    public void setProductByProductId2Fk(ProductEntity productByProductId2Fk)
    {
        this.productByProductId2Fk = productByProductId2Fk;
    }
}
