package Root.Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "transaction", schema = "mediastore", catalog = "postgres")
public class TransactionEntity {
    private long transactionId;
    private String street;
    private String zip;
    private String bankAccountNumber;
    private String addressee;
    private transient Collection<CartEntity> cartsByTransactionId;
    private UserEntity userByUserName;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id", nullable = false)
    public long getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(long transactionId)
    {
        this.transactionId = transactionId;
    }

    @Basic
    @Column(name = "street", nullable = false, length = 50)
    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    @Basic
    @Column(name = "zip", nullable = false, length = 50)
    public String getZip()
    {
        return zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

    @Basic
    @Column(name = "bank_account_number", nullable = false, length = 50)
    public String getBankAccountNumber()
    {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber)
    {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Basic
    @Column(name = "addressee", nullable = false, length = 50)
    public String getAddressee()
    {
        return addressee;
    }

    public void setAddressee(String addressee)
    {
        this.addressee = addressee;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionEntity that = (TransactionEntity) o;
        return transactionId == that.transactionId &&
                Objects.equals(street, that.street) &&
                Objects.equals(zip, that.zip) &&
                Objects.equals(bankAccountNumber, that.bankAccountNumber) &&
                Objects.equals(addressee, that.addressee);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(transactionId, street, zip, bankAccountNumber, addressee);
    }

    @OneToMany(mappedBy = "transactionByTransactionIdFk")
    public Collection<CartEntity> getCartsByTransactionId()
    {
        return cartsByTransactionId;
    }

    public void setCartsByTransactionId(Collection<CartEntity> cartsByTransactionId)
    {
        this.cartsByTransactionId = cartsByTransactionId;
    }

    @ManyToOne
    @JoinColumn(name = "username", referencedColumnName = "username", nullable = false)
    public UserEntity getUserByUserName()
    {
        return userByUserName;
    }

    public void setUserByUserName(UserEntity userByUserName)
    {
        this.userByUserName = userByUserName;
    }
}
