package Root.Entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "book", schema = "mediastore", catalog = "postgres")
public class BookEntity {
    private long bookId;
    private Integer numberOfPages;
    private Date releaseDate;
    private String isbn;
    private transient List<PublisherEntity> publisherEntityList;
    private transient ProductEntity productEntity;

    @Id
    @Column(name = "book_id", nullable = false)
    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    @Basic
    @Column(name = "number_of_pages", nullable = true)
    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    @Basic
    @Column(name = "release_date", nullable = true)
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Basic
    @Column(name = "isbn", nullable = true, length = 13)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @ManyToMany
    @JoinTable(
            name = "mediastore.publisher_book",
            joinColumns = { @JoinColumn(name = "book_id") },
            inverseJoinColumns = { @JoinColumn(name = "publisher_id") }
    )
    public List<PublisherEntity> getPublisherEntityList() {
        return publisherEntityList;
    }

    public void setPublisherEntityList(List<PublisherEntity> publisherEntityList) {
        this.publisherEntityList = publisherEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookEntity that = (BookEntity) o;
        return bookId == that.bookId && Objects.equals(numberOfPages, that.numberOfPages) && Objects.equals(releaseDate, that.releaseDate) && Objects.equals(isbn, that.isbn) && Objects.equals(publisherEntityList, that.publisherEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId, numberOfPages, releaseDate, isbn, publisherEntityList);
    }

    @OneToOne
    @JoinColumn(name = "book_id")
    @MapsId
    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }
}
