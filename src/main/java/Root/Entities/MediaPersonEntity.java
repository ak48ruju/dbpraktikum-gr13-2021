package Root.Entities;

import javax.persistence.*;
import java.util.Objects;

@NamedQuery(name = "media_person.byName",
    query = """
    SELECT mpe
    FROM MediaPersonEntity mpe
    WHERE mpe.name= ?1
    """
)
@Entity
@Table(name = "media_person", schema = "mediastore", catalog = "postgres")
public class MediaPersonEntity {
    private long mediaPersonId;
    private String name;
    //private MediaProductionRoleEntity mediaProductionRoleEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "media_person_id")
    public long getMediaPersonId() {
        return mediaPersonId;
    }

    public void setMediaPersonId(long mediaPersonId) {
        this.mediaPersonId = mediaPersonId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaPersonEntity that = (MediaPersonEntity) o;
        return mediaPersonId == that.mediaPersonId && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mediaPersonId, name);
    }
}
