package Root.ErrorLogging;

import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ErrorLogger {

    private Document errorDoc;
    private final File errorFile;
    private DocumentBuilder builder;
    private String timeStampAtInit;
    private final Element logs;
    private Element errorLogElement;
    private final XPath xPath = XPathFactory.newInstance().newXPath();
    private final String prefixErrorStatisticsXPath;

    public ErrorLogger(String pathToLogFile)
    {
        this.errorFile = new File(pathToLogFile);
        setBuilder();
        setDocument();
        setTimeStamp();
        prefixErrorStatisticsXPath = "//log[@Timestamp=\"" + timeStampAtInit + "\"]/";
        logs = errorDoc.getDocumentElement();
        appendThisErrorLogAtLogs();
    }

    /**
     * Generates a new logs/log/entry
     *
     * @param persistenceException which caused the transaction to fail
     * @param entity               which was to be persisted by entityManger
     */
    public void logError(PersistenceException persistenceException, Object entity)
    {

        if (persistenceException.getMessage().equals("org.hibernate.exception.ConstraintViolationException: could not execute statement")) {
            ConstraintViolationException exception = (ConstraintViolationException) persistenceException.getCause();
            String type = exception.getClass().getCanonicalName();
            String constraintName = getConstraintName(exception.getSQLException().getMessage());
            String cause = exception.getSQLException().getMessage().replace("\n", "\t");
            String entityString = entity.getClass().getCanonicalName();

            createLogEntry(type, constraintName, cause, entityString);


        } else if (persistenceException.getMessage().equals("org.hibernate.exception.DataException: could not execute statement")) {

            DataException exception = (DataException) persistenceException.getCause();

            String type = exception.getClass().getCanonicalName();
            String constraintName = "DATA CONSTRAINT";
            String cause = exception.getSQLException().getMessage().replace("\n", "\t");
            String entityString = entity.getClass().getCanonicalName();

            createLogEntry(type, constraintName, cause, entityString);
        } else if (persistenceException.getCause().getClass().equals(PropertyValueException.class)) {
            PropertyValueException exception = (PropertyValueException) persistenceException.getCause();

            String type = exception.getClass().getCanonicalName();
            String constaintName = "DATA CONSTRAINT";
            String cause = exception.getMessage();
            String enityString = exception.getEntityName();

            createLogEntry(type, constaintName, cause, enityString);
        } else {
            System.out.println(" exception - FIX NOW");
            //System.exit(-1);
            String type = "unfiltered Rollback";
            String constaintName = "---";
            String cause = persistenceException.getMessage();
            String enityString = "---";

            createLogEntry(type, constaintName, cause, enityString);
        }
    }

    private void createLogEntry(String type, String constraintName, String cause, String entity)
    {
        Element entry = errorDoc.createElement("entry");
        errorLogElement.appendChild(entry);

        Element exceptionElement = createAndAppendElement(entry, "exception", "");

        exceptionElement.setAttribute("type", type);
        createAndAppendElement(exceptionElement, "constraintName", constraintName);
        createAndAppendElement(exceptionElement, "cause", cause);
        createAndAppendElement(exceptionElement, "entity", entity);
    }

    public void logError(EntityExistsException entityExistsException, Object entity)
    {
        System.out.println(entityExistsException + "\n" + entity);
    }

    /**
     * creates and appends generated child Element
     *
     * @param parent      of the child
     * @param tagName     tagname of child
     * @param textContent text content of child
     * @return child
     */
    private Element createAndAppendElement(Element parent, String tagName, String textContent)
    {
        Element child = errorDoc.createElement(tagName);
        child.setTextContent(textContent);
        parent.appendChild(child);
        return child;
    }


    private void setDocument()
    {
        try {
            errorDoc = builder.parse(errorFile);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

    }

    private void setBuilder()
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void setTimeStamp()
    {
        Date date = new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        timeStampAtInit = ts.toString().replace(" ", "@");
    }

    private void appendThisErrorLogAtLogs()
    {
        errorLogElement = errorDoc.createElement("log");
        errorLogElement.setAttribute("Timestamp", timeStampAtInit);
        logs.appendChild(errorLogElement);
    }

    /**
     * Writes log results into errorLog.xml
     */
    public void writeToLogFile()
    {
        try {
            Source source = new DOMSource(errorDoc);
            StreamResult result = new StreamResult(new OutputStreamWriter(
                    new FileOutputStream(errorFile), StandardCharsets.UTF_8));
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(source, result);
        } catch (FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public void printStatistics()
    {
        generateAndPrintErrorStatistic("Root.Entities.PublisherEntity", new String[]{"Unique-Constraint »publisher_pk«"});
        generateAndPrintErrorStatistic("Root.Entities.ShopEntity", new String[]{"Unique-Constraint »shop_pk«"});
        generateAndPrintErrorStatistic("Root.Entities.OfferEntity", new String[]{"Unique-Constraint »condition_pk«",
                "Unique-Constraint »condition_unique_offer«",
                "Check-Constraint »condition_currency_check«",
                "Check-Constraint »condition_price_check«",
                "Check-Constraint »condition_price_multiplier_check«",
                "Check-Constraint »condition_state_check«",
                "DATA CONSTRAINT"});
        generateAndPrintErrorStatistic("Root.Entities.ReviewEntity", new String[]{"Unique-Constraint »review_pk«",
                "Unique-Constraint »review_unique_constraint«",
                "Check-Constraint »review_helpful_check«",
                "Check-Constraint »review_rating_check«",
                "DATA CONSTRAINT"});
        generateAndPrintErrorStatistic("Root.Entities.ProductEntity", new String[]{"Unique-Constraint »product_pk«",
                "DATA CONSTRAINT",
                "Unique-Constraint »product_ean_uindex«",
                "Unique-Constraint »product_asin_uindex«",
                "Check-Constraint »product_product_group_check«"});
        generateAndPrintErrorStatistic("Root.Entities.BookEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.DvdEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.MusicCdEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.SimilarProductsEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.PublisherEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.LabelEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.SongEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.CategoryEntity", new String[]{});
        generateAndPrintErrorStatistic("Root.Entities.CategoryEntryEntity", new String[]{});
    }

    private void generateAndPrintErrorStatistic(String entityName, String[] constraintArray)
    {
        String query1 = getErrorsWhilePersistingEntity(entityName);
        System.out.println("################################################################");
        System.out.println(
                "TOTAL ERRORS WHILE PERSISTING "
                        + entityName
                        + ": "
                        + Objects.requireNonNull(xPathQuery(query1)).getLength());

        for (String s : constraintArray) {
            String queryConstraint = getErrorsOfConstraintName(entityName, s);
            System.out.println(
                    "CONSTRAINT VIOLATION: "
                            + s
                            + " - count: "
                            + Objects.requireNonNull(xPathQuery(queryConstraint)).getLength());
        }
    }

    private String getErrorsWhilePersistingEntity(String fullEntityName)
    {
        return prefixErrorStatisticsXPath
                + "entry/exception/entity[text()=\""
                + fullEntityName
                + "\"]";
    }

    private String getErrorsOfConstraintName(String fullEntityName, String constraintName)
    {
        return prefixErrorStatisticsXPath
                + "entry/exception/entity[text()=\""
                + fullEntityName
                + "\"]/../constraintName[text() = \""
                + constraintName
                + "\"]";
    }


    private NodeList xPathQuery(String xPathPath)
    {
        try {
            return (NodeList) xPath.compile(xPathPath).evaluate(errorLogElement, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getConstraintName(String message)
    {
        String pattern = "(?<=verletzt )(.*)";
        Pattern reg = Pattern.compile(pattern);
        Matcher matcher = reg.matcher(message);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "unknown";
        }
    }

}
