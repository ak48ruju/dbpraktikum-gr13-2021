package Root;

import Root.DataImport.ImportController;
import Root.DatabaseOperations.DatabaseClient;
import Root.DatabaseOperations.EndpointCommands;
import Root.Entities.OfferEntity;
import Root.Entities.ProductEntity;
import Root.Entities.ReviewEntity;
import Root.Entities.UserEntity;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class EndpointService {
    private final EndpointCommands endpointCommands;
    private DatabaseClient databaseClient;

    public EndpointService(EndpointCommands endpointCommands)
    {
        this.endpointCommands = endpointCommands;
    }

    /**
     * Endpoint method structure to execute client requests
     * @param jsonObject is the received request from the client
     * @return a json string which will be sent to the client
     */
    public String executeCommand(JSONObject jsonObject)
    {

        String errorMessage = "";
        Object returnObject = new Object();
        final String command = jsonObject.getString("Command");
        final JSONObject arguments = jsonObject.getJSONObject("Arguments");


        if (Objects.isNull(databaseClient) && !command.equals("init")) {
            errorMessage = "databaseClient is null";
        } else {

            switch (command) {
                case "init" -> {
                    databaseClient = endpointCommands.init(
                            arguments.getString("DbUser"),
                            arguments.getString("DbPass")
                    );
                    if (!databaseClient.isConnected()) {
                        errorMessage = "Couldn't connect to database";
                    }
                }
                case "finish" -> {
                    endpointCommands.finish(databaseClient);
                    databaseClient = null;
                }
                case "getProduct" -> {
                    ProductEntity productEntity = endpointCommands.getProduct(
                            databaseClient,
                            arguments.getString("Asin")
                    );
                    if (!Objects.isNull(productEntity)) {
                        returnObject = productEntity;
                    }
                }
                case "getProducts" -> {
                    final List<ProductEntity> productEntityList = endpointCommands.getProducts(
                            databaseClient,
                            arguments.getString("Pattern")
                    );
                    returnObject = productEntityList;
                }
                case "getCategoryTree" -> {
                    final Map<String, Object> categoryTree = endpointCommands.getCategoryTree(databaseClient);
                    returnObject = categoryTree;
                }
                case "getProductsByCategoryPath" -> {
                    final List<ProductEntity> productEntityList = endpointCommands.getProductsByCategoryPath(
                            databaseClient,
                            arguments.getString("Path")
                    );
                    returnObject = productEntityList;
                }
                case "getTopProducts" -> {
                    Integer topK = 0;
                    try {
                        topK = Integer.parseInt(arguments.getString("K"));
                        final List<ProductEntity> productEntityList = endpointCommands.getTopProducts(
                                databaseClient,
                                topK
                        );
                        returnObject = productEntityList;
                    } catch (Exception e) {
                        errorMessage = e.getMessage();
                    }
                }
                case "getSimilarCheaperProducts" -> {
                    final List<OfferEntity> offerList = endpointCommands.getSimilarCheaperProduct(
                            databaseClient,
                            arguments.getString("Asin")
                    );
                    if(offerList.isEmpty()){
                        errorMessage = "Could find any cheaper similar product";
                    }
                    returnObject = offerList;
                }
                case "addNewReview" -> {
                    try {
                        final ReviewEntity reviewEntity = endpointCommands.addNewReview(
                                databaseClient,
                                Integer.parseInt(arguments.getString("Rating")),
                                new Date(System.currentTimeMillis()),
                                0,
                                arguments.getString("Summary"),
                                arguments.getString("ContentReview"),
                                arguments.getString("Username"),
                                arguments.getString("Asin")
                        );
                        if (Objects.isNull(reviewEntity)) {
                            errorMessage = "could not persist review";
                        }
                    } catch (Exception e) {
                        errorMessage = e.getMessage();
                    }
                }
                case "viewReviews" -> {
                    final List<ReviewEntity> reviewEntityList = endpointCommands.viewReviews(
                            databaseClient,
                            arguments.getString("Asin")
                    );
                    returnObject = reviewEntityList;

                }
                case "getTrolls" -> {
                    try {
                        Double rating = Double.parseDouble(arguments.getString("Rating"));
                        Long minimumAmount = Long.parseLong(arguments.getString("MinimumAmount"));
                        final List<UserEntity> userEntityList = endpointCommands.getTrolls(
                                databaseClient,
                                rating,
                                minimumAmount
                        );
                    returnObject = userEntityList;
                    } catch (Exception e) {
                        errorMessage = e.getMessage();
                    }
                }
                case "getOffers" -> {
                    List<OfferEntity> offerEntityList = endpointCommands.getOffers(
                            databaseClient,
                            arguments.getString("Asin")
                    );
                    returnObject = offerEntityList
                            .stream()
                            .filter(offerEntity -> offerEntity.getAvailability().equals("available"))
                            .collect(Collectors.toList());

                }
                case "importData" -> {
                    ImportController importController = new ImportController(
                            databaseClient);
                    importController.importData(
                            true,
                            true,
                            true,
                            true);
                }
            }
        }

        String responseMessage = composeResponseJsonString(returnObject, errorMessage, jsonObject);
        return responseMessage;
    }

    /**
     * Composes the json string which will be sent to the client
     * @param queryResult of the endpoint query
     * @param error which occurred otherwise an empty string
     * @param jsonObject of the client request
     * @return a formatted json string
     */
    private String composeResponseJsonString(Object queryResult, String error, JSONObject jsonObject)
    {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("Command", jsonObject.getString("Command"));
        jsonResponse.put("Arguments", jsonObject.getJSONObject("Arguments"));
        jsonResponse.put("Error", error);

        String queryResultJson = new Gson().toJson(queryResult);
        if (queryResultJson.startsWith("{")) {
            jsonResponse.put("Result", new JSONObject(new Gson().toJson(queryResult)));
        } else {
            jsonResponse.put("Result", new JSONArray(new Gson().toJson(queryResult)));
        }
        return jsonResponse.toString();
    }

    /**
     * An explicit finish command to close the database connection
     */
    public void explicitFinishCommand()
    {
        endpointCommands.finish(databaseClient);
        databaseClient = null;
    }
}
