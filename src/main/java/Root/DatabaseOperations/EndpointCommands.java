package Root.DatabaseOperations;

import Root.Entities.OfferEntity;
import Root.Entities.ProductEntity;
import Root.Entities.ReviewEntity;
import Root.Entities.UserEntity;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface EndpointCommands {
    DatabaseClient init(String dbUser, String dbPass);
    void finish(DatabaseClient databaseClient);
    ProductEntity getProduct (DatabaseClient databaseClient, String asin);
    List<ProductEntity> getTopProducts (DatabaseClient databaseClient, Integer k);
    List<ProductEntity> getProducts (DatabaseClient databaseClient, String pattern);
    Map<String, Object> getCategoryTree (DatabaseClient databaseClient);
    List<ProductEntity> getProductsByCategoryPath (DatabaseClient databaseClient, String path);
    List<OfferEntity> getSimilarCheaperProduct (DatabaseClient databaseClient, String asin);
    ReviewEntity addNewReview(DatabaseClient databaseClient, Integer rating, Date date, Integer helpful, String summary, String contentReview, String username, String asin) throws Exception;
    List<ReviewEntity> viewReviews (DatabaseClient databaseClient, String asin);
    List<UserEntity> getTrolls (DatabaseClient databaseClient, Double rating, Long minimumAmount);
    List<OfferEntity> getOffers (DatabaseClient databaseClient, String asin);



}
