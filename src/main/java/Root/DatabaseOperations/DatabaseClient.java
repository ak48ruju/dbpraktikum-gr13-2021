package Root.DatabaseOperations;

import Root.ErrorLogging.ErrorLogger;
import Root.QueryFactory.Queryable;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class used to connect to and execute database instructions
 */
public class DatabaseClient {

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    private final ErrorLogger errorLogger;
    private boolean connected;


    public DatabaseClient()
    {
        this.connected = false;
        this.errorLogger = new ErrorLogger(System.getProperty("user.dir") + "/src/main/resources/errorLog.xml");
    }

    public boolean isConnected()
    {
        return connected;
    }



    /**
     *
     * @param entity
     * @return true if persist is successful | false otherwise
     */
    public boolean persistEntity(Object entity)
    {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        try {
            entityManager.persist(entity);
            transaction.commit();
            return true;
        } catch (PersistenceException e1) {
            errorLogger.logError(e1, entity);
            return false;
        } catch (Exception e) {
            System.out.println("Unhandled exception - should fix databaseclient.persistEntity()");
            return false;
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }


    /**
     * persistEntities is a non greedy method to persist entity lists. Errors are expected
     * and are dealt with.
     * In principle entities are persisted in groups of *batchSize* lists. If an entity
     * violates a constraint the transaction rolls back and the list is separated at the
     * error entity and another import try occurs without said error entity.
     * @param entities list
     * @return
     */
    public void persistEntities(List<Object> entities)
    {
        int batchSize = 1000;
        int batchCounter = 0;
        List<Object> batchList;
        while (true){
            if(entities.size()<= (batchCounter+1)*batchSize){
                batchList = entities.subList(batchCounter*batchSize, entities.size());
                tryPersistEntities(batchList);
                return;
            } else {
                batchList = entities.subList(batchCounter*batchSize, (batchCounter+1)*batchSize);
                tryPersistEntities(batchList);
            }
            batchCounter++;


        }
    }

    private void tryPersistEntities(List<Object> entities){
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.flush();
        int elementCounter = 0;
        try {
            for(Object entity: entities){
                entityManager.persist(entity);
                elementCounter++;
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }

            if(e instanceof PersistenceException){
                errorLogger.logError((PersistenceException)e, entities.get(elementCounter));
            } else {
                System.out.println("UNKNOWN ERROR WHILE PERSISTING: " + e);
            }
            if(elementCounter!=0){
                tryPersistEntities(entities.subList(0, elementCounter));
            }
            if(elementCounter != entities.size()-1){
                tryPersistEntities(entities.subList(elementCounter+1, entities.size()));
            }

        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    /**
     *
     * @param entity
     * @return true if remove is successful | false otherwise
     */
    public boolean removeEntity(Object entity)
    {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.remove(entity);
            transaction.commit();
            return true;
        } catch (PersistenceException e1) {
            errorLogger.logError(e1, entity);
            return false;
        } catch (Exception e) {
            System.out.println("Unhandled exception - should fix databaseclient.persistEntity()");
            return false;
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    public void mergeEntity(Object entity)
    {

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.merge(entity);
            transaction.commit();

        } catch (PersistenceException e1) {
            errorLogger.logError(e1, entity);

        } catch (Exception e) {
            System.out.println("Unhandled exception - should fix databaseclient.persistEntity()");
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    /**
     * Query is a templated method for executing queries
     *
     * @param queryFactory constructs the specific query to be executed
     * @param <T>          specifies which entity is being queried
     * @return Resultlist of queried entities
     */
    public <T> List<T> executeQuery(Queryable<T> queryFactory)
    {
        return queryFactory.execute(entityManager);
    }

    /**
     * Instates a connection to the database and used persistance unit
     */
    public void establishConnection(String dbUser, String dbPass)
    {
        try {
            // https://stackoverflow.com/questions/18583881/changing-persistence-unit-dynamically-jpa
            // Keep the persistence unit file (persistence.xml) as it's. You can override the properties in it as follows.

            Map<String, String> persistenceMap = new HashMap<String, String>();

            persistenceMap.put("javax.persistence.jdbc.user", dbUser);
            persistenceMap.put("javax.persistence.jdbc.password", dbPass);

            entityManagerFactory = Persistence.createEntityManagerFactory("mediastore", persistenceMap);
            entityManager = entityManagerFactory.createEntityManager();
            connected = true;

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Closes an established connection
     */
    public void closeConnection()
    {
        entityManager.close();
        entityManagerFactory.close();
        connected = false;
    }

    public ErrorLogger getErrorLogger()
    {
        return errorLogger;
    }

    public EntityManager getEntityManager()
    {
        return entityManager;
    }


}
