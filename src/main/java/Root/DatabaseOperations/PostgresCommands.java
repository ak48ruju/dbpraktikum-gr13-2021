package Root.DatabaseOperations;

import Root.Entities.*;
import Root.EntityFactory.*;

import java.sql.Date;
import java.util.*;

public class PostgresCommands implements EndpointCommands {


    @Override
    public DatabaseClient init(String dbUser, String dbPass)
    {
        DatabaseClient databaseClient = new DatabaseClient();
        databaseClient.establishConnection(dbUser, dbPass);
        return databaseClient;
    }

    @Override
    public void finish(DatabaseClient databaseClient)
    {
        databaseClient.closeConnection();
    }

    @Override
    public ProductEntity getProduct(DatabaseClient databaseClient, String asin)
    {
        ProductEntityFactory productEntityFactory = new ProductEntityFactory();
        ProductEntity productEntity = productEntityFactory.getProductByAsin(databaseClient, asin);

        return productEntity;
    }


    @Override
    public List<ProductEntity> getTopProducts(DatabaseClient databaseClient, Integer k)
    {
        ProductEntityFactory productEntityFactory = new ProductEntityFactory();
        List<ProductEntity> productEntityList = productEntityFactory.getProductsByTopKRank(databaseClient, k);

        return productEntityList;
    }

    @Override
    public List<ProductEntity> getProducts(DatabaseClient databaseClient, String pattern)
    {
        ProductEntityFactory productEntityFactory = new ProductEntityFactory();
        List<ProductEntity> productEntityList = productEntityFactory.getProductsByTitlePattern(databaseClient, pattern);

        return productEntityList;
    }

    @Override
    public Map<String, Object> getCategoryTree(DatabaseClient databaseClient)
    {
        Map<String, Object> categoryTree = new HashMap<>();
        CategoryEntityFactory categoryEntityFactory = new CategoryEntityFactory();

        final List<CategoryEntity> rootCategories = categoryEntityFactory.getRootCategories(databaseClient);
        for (CategoryEntity rootCat : rootCategories) {
            constructCategoryTree(databaseClient, rootCat, categoryTree);
        }

        return categoryTree;
    }

    /**
     * Recursion to construct category tree
     * - depth first approach with map<String, Object> structure
     * - leaf nodes are assigned an empty object
     *
     * @param databaseClient
     * @param parentCategory
     * @param categoryMapToFill
     */
    private void constructCategoryTree(DatabaseClient databaseClient, CategoryEntity parentCategory, Map<String, Object> categoryMapToFill)
    {
        CategoryEntityFactory categoryEntityFactory = new CategoryEntityFactory();
        final List<CategoryEntity> childCategories = categoryEntityFactory.getChildCategories(databaseClient, parentCategory.getCategoryId());
        if (childCategories.isEmpty()) {
            categoryMapToFill.put(parentCategory.getCategoryName(), new Object());
            return;
        }
        List<Map<String, Object>> childMaps = new ArrayList<>();
        for (CategoryEntity childCategory : childCategories) {
            HashMap<String, Object> childCategoryMap = new HashMap<>();
            constructCategoryTree(databaseClient, childCategory, childCategoryMap);
            childMaps.add(childCategoryMap);
        }
        categoryMapToFill.put(parentCategory.getCategoryName(), childMaps);
    }

    /**
     * Matches the path with a path% pattern to extract all Categories and Subcategories.
     * Then all CategoryEntries are collected which then contain the products to be returned
     *
     * @param databaseClient a database client
     * @param path           CategoryName
     * @return a List of found ProductEntities
     */
    @Override
    public List<ProductEntity> getProductsByCategoryPath(DatabaseClient databaseClient, String path)
    {
        CategoryEntityFactory categoryEntityFactory = new CategoryEntityFactory();
        HashSet<ProductEntity> products = new HashSet<>();
        final List<CategoryEntity> categoriesAndSubCategoriesByPath = categoryEntityFactory.getCategoriesAndSubCategoriesByPath(databaseClient, path);
        for (CategoryEntity categoryEntity : categoriesAndSubCategoriesByPath) {
            final Collection<CategoryEntryEntity> categoryEntriesByCategoryId = categoryEntity.getCategoryEntriesByCategoryId();
            for (CategoryEntryEntity categoryEntryEntity : categoryEntriesByCategoryId) {
                products.add(categoryEntryEntity.getProductByProductIdFk());
            }
        }
        return new ArrayList<>(products);
    }

    /**
     * Returns a list of all products similar to the main product that have a cheaper offer
     * @param databaseClient
     * @param asin of the main product
     * @return Returns a list of OfferEntities
     */
    @Override
    public List<OfferEntity> getSimilarCheaperProduct(DatabaseClient databaseClient, String asin)
    {
        final ProductEntityFactory productEntityFactory = new ProductEntityFactory();
        List<OfferEntity> cheaperSimilarProducts = new ArrayList<>();

        final ProductEntity mainProduct = productEntityFactory.getProductByAsin(databaseClient, asin);
        final Collection<OfferEntity> offersOfMainProduct = mainProduct.getOffersByProductId();
        OfferEntity cheapestOfferMainProduct = getCheapestOfferInCollection(offersOfMainProduct);

        if (Objects.isNull(cheapestOfferMainProduct)) {
            return null;
        }
        List<ProductEntity> similarProducts = getSimilarProducts(mainProduct, databaseClient);
        for (ProductEntity similarProduct : similarProducts) {
            OfferEntity cheapestOfferSimilarProduct = getCheapestOfferInCollection(similarProduct.getOffersByProductId());
            if (offerBIsCheaperThanOfferA(cheapestOfferMainProduct, cheapestOfferSimilarProduct)) {
                cheaperSimilarProducts.add(cheapestOfferSimilarProduct);
            }
        }
        return cheaperSimilarProducts;
    }

    /**
     * Finds all similar products to a main product
     *
     * @param mainProduct    which is contained in all SimilarProductEntites
     * @param databaseClient
     * @return a List of ProductEntities which are similar
     */
    private List<ProductEntity> getSimilarProducts(ProductEntity mainProduct, DatabaseClient databaseClient)
    {
        final SimilarProductsEntityFactory similarProductsEntityFactory = new SimilarProductsEntityFactory();
        List<ProductEntity> similarProducts = new ArrayList<>();

        final List<SimilarProductsEntity> similarProductsEntityOfMainProduct =
                similarProductsEntityFactory.getSimilarProductsEntityById(databaseClient, mainProduct);

        for (SimilarProductsEntity similarProductsEntity : similarProductsEntityOfMainProduct) {
            if (similarProductsEntity.getProductByProductId1Fk().getAsin().equals(mainProduct.getAsin())) {
                similarProducts.add(similarProductsEntity.getProductByProductId2Fk());
            } else {
                similarProducts.add(similarProductsEntity.getProductByProductId1Fk());
            }
        }
        return similarProducts;
    }

    /**
     * Gets the offer with the lowest price in the given OfferEntity Collection
     *
     * @param offerEntityCollection
     * @return cheapest offer if present otherwise null
     */
    private OfferEntity getCheapestOfferInCollection(Collection<OfferEntity> offerEntityCollection)
    {
        OfferEntity cheapestOffer = null;
        for (OfferEntity currentOffer : offerEntityCollection) {
            if (Objects.isNull(cheapestOffer)) {
                cheapestOffer = currentOffer;
            } else if (offerBIsCheaperThanOfferA(cheapestOffer, currentOffer)) {
                cheapestOffer = currentOffer;
            }
        }
        return cheapestOffer;
    }

    /**
     * Asserts whether offer a is cheaper than offer b.
     * Anything is cheaper than a not available offer.
     * A not available offer is not cheaper than a not available offer
     * If both offers have a price tag the lower price is the cheaper one
     *
     * @param a first offer
     * @param b second offer
     * @return true if b is cheaper than a
     */
    private boolean offerBIsCheaperThanOfferA(OfferEntity a, OfferEntity b)
    {
        if (Objects.isNull(a.getPrice()) && Objects.isNull(b.getPrice())) {
            return false;
        } else if (Objects.nonNull(a.getPrice()) && Objects.isNull(b.getPrice())) {
            return false;
        } else if (Objects.isNull(a.getPrice()) && Objects.nonNull(b.getPrice())) {
            return true;
        } else {
            return b.getPrice() < a.getPrice();
        }
    }

    @Override
    public ReviewEntity addNewReview(DatabaseClient databaseClient, Integer rating, Date date, Integer helpful, String summary, String contentReview, String username, String asin) throws Exception
    {
        ReviewEntityFactory reviewEntityFactory = new ReviewEntityFactory();
        ReviewEntity reviewEntity = reviewEntityFactory.addNewRewiew(databaseClient, rating, date, helpful, summary, contentReview, username, asin);

        return reviewEntity;
    }

    @Override
    public List<ReviewEntity> viewReviews(DatabaseClient databaseClient, String asin)
    {
        ReviewEntityFactory reviewEntityFactory = new ReviewEntityFactory();
        List<ReviewEntity> reviewEntityList = reviewEntityFactory.getReviewsByAsin(databaseClient, asin);

        return reviewEntityList;
    }

    @Override
    public List<UserEntity> getTrolls(DatabaseClient databaseClient, Double rating, Long minimumAmount)
    {
        UserEntityFactory userEntityFactory = new UserEntityFactory();
        List<UserEntity> userEntityList = userEntityFactory.getTrolls(databaseClient, rating, minimumAmount);

        return userEntityList;
    }

    @Override
    public List<OfferEntity> getOffers(DatabaseClient databaseClient, String asin)
    {
        OfferEntityFactory offerEntityFactory = new OfferEntityFactory();
        List<OfferEntity> offerEntityList = offerEntityFactory.getOffersByAsin(databaseClient, asin);

        return offerEntityList;
    }

}
