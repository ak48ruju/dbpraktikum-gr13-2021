package Root;

import Root.DatabaseOperations.EndpointCommands;
import Root.DatabaseOperations.PostgresCommands;
import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private ServerSocket serverSocket;
    private BufferedReader inputReader;
    private PrintStream outputWriter;
    private Socket clientSocket;
    private final int bufferSize; //for message length
    private boolean hasConnection = false;

    EndpointCommands endpointCommands;
    EndpointService endpointService;

    public Server(int bufferSize)
    {
        this.bufferSize = bufferSize;
        this.endpointCommands = new PostgresCommands();
        this.endpointService   = new EndpointService(endpointCommands);
        try {
            serverSocket = new ServerSocket(50001);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Accepts a new client connection
     * @throws IOException
     */
    private void acceptNewConnection() throws IOException
    {
        clientSocket = serverSocket.accept();
        outputWriter = new PrintStream(new BufferedOutputStream(clientSocket.getOutputStream()));
        inputReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        hasConnection = true;
        System.out.println("Establishing new connection");
    }

    /**
     * Convert a received client json message to a JSONObject
     * @param request is the received client string
     * @return the json string as JSONObject
     */
    private JSONObject convertMessageToJsonObject(String request)
    {
        JSONObject jsonObject = new JSONObject(request);
        System.out.println(jsonObject);
        return jsonObject;
    }

    /**
     * Receive a message from the connected client
     * @return client message as String
     * @throws IOException if buffer size < than client request
     */
    private String receiveMessage() throws IOException
    {
        char[] buffer = new char[bufferSize];
        int length = inputReader.read(buffer, 0, bufferSize);
        return new String(buffer, 0, length);

    }

    /**
     * Handles Sending a message to the client. If the message exceeds the available buffer size
     * multiple messages will be sent to the client until everything is transmitted
     * @param message json message to be sent to the client
     * @throws IOException
     */
    private void sendMessage(String message) throws IOException
    {
        System.out.println(message);
        boolean sendMessageComplete = false;
        int msgCounter = 0;
        while(!sendMessageComplete) {

            int subStringStart = msgCounter * bufferSize;
            int subStringEnd = Math.min(subStringStart + bufferSize, message.length());

            outputWriter.print(message.substring(subStringStart, subStringEnd));
            outputWriter.flush();

            String answer = receiveMessage();
            if(answer.equals("ack") && subStringEnd == message.length()){
                sendMessageComplete = true;
            } else if (answer.equals("ack")) {
                msgCounter++;
            }
        }
        outputWriter.print("fin");
        outputWriter.flush();

    }

    /**
     * Main function for running the server
     */
    public void run()
    {
        try {
            while (true) {
                if (!hasConnection) {
                    acceptNewConnection();
                }
                String message = receiveMessage();
                String responseJson = endpointService.executeCommand(convertMessageToJsonObject(message));
                sendMessage(responseJson);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                clientSocket.close();
                endpointService.explicitFinishCommand();
                System.out.println("closing connection");
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                hasConnection = false;
                this.run();
            }
        }
    }
}
