package Root.DataImport;


import Root.DatabaseOperations.DatabaseClient;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class ImportController {

    private final String pathToDresden_nowhitespace_xml= System.getProperty("user.dir") + "/src/main/resources/dresden.xml";
    private final String pathToLeizig_transformed_nowhitespace_xml= System.getProperty("user.dir") + "/src/main/resources/leipzig_transformed.xml";
    private final String pathToReviews_xml= System.getProperty("user.dir") + "/src/main/resources/reviews.xml";
    private final String pathToCategories_xml= System.getProperty("user.dir") + "/src/main/resources/categories.xml";


    private final ReviewsImportController reviewsImportController = new ReviewsImportController();;
    private final ShopDataImportController shopDataImportController = new ShopDataImportController();
    private final CategoryImportController categoryImportController = new CategoryImportController();
    private DocumentBuilder builder;
    private final DatabaseClient databaseClient;

    public ImportController(DatabaseClient databaseClient)
    {
        this.databaseClient = databaseClient;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }


    }

    /**
     * Main method to control data import from all xml datasources
     */
    public void importData(boolean importShopLeipzig, boolean importShopDresden, boolean importCategories
            , boolean importReviews) {
        try {
            //databaseClient.establishConnection();
            if (importShopLeipzig) {
                shopDataImportController.importData(builder.parse(pathToLeizig_transformed_nowhitespace_xml), databaseClient);
            }
            if (importShopDresden) {
                shopDataImportController.importData(builder.parse(pathToDresden_nowhitespace_xml), databaseClient);
            }
            if (importCategories) {
                //databaseClient.closeConnection();
                //databaseClient.establishConnection();
                categoryImportController.importData(builder.parse(pathToCategories_xml), databaseClient);
            }
            if (importReviews) {
                //databaseClient.closeConnection();
                //databaseClient.establishConnection();
                reviewsImportController.importData(builder.parse(pathToReviews_xml), databaseClient);
            }
            databaseClient.getErrorLogger().printStatistics();

        } catch (SAXException | IOException e) {
            e.printStackTrace();
        } finally {
            databaseClient.getErrorLogger().writeToLogFile();
            //databaseClient.closeConnection();
        }
    }
}
