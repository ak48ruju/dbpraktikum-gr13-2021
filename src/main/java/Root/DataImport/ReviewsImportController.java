package Root.DataImport;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.ProductEntity;
import Root.Entities.ReviewEntity;
import Root.Entities.UserEntity;
import Root.EntityFactory.ReviewEntityFactory;
import Root.EntityFactory.UserEntityFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReviewsImportController extends XmlDocumentImport {

    ReviewEntityFactory reviewEntityFactory = new ReviewEntityFactory();
    UserEntityFactory userEntityFactory = new UserEntityFactory();


    public ReviewsImportController()
    {

    }

    /**
     * Main method to control data import from reviews_nowhitespace.xml
     */
    public void importData(Document doc, DatabaseClient databaseClient)
    {

        NodeList itemList = getItemNodeList(doc, "/reviews/review");
        ProductEntity productOfReviewDb;
        UserEntity userOfReview;
        UserEntity userOfReviewDb;
        ReviewEntity reviewEntity;
        List<Object> guestReviews = new ArrayList<>();

        for (int i = 0; i < itemList.getLength(); i++) {
            Node item = itemList.item(i);

            userOfReview = userEntityFactory.generateUserByDocument(item);
            if (!userEntityFactory.userExists(databaseClient, userOfReview.getUsername())) {
                databaseClient.persistEntity(userOfReview);
            }

            userOfReviewDb = userEntityFactory.getUserByUserName(databaseClient, userOfReview.getUsername());
            reviewEntity = reviewEntityFactory.generateReviewEntityByNode
                    (databaseClient, item, userOfReviewDb);
            if (Objects.isNull(reviewEntity.getProductByProductIdFk())) {
                continue;
            }

            productOfReviewDb = reviewEntity.getProductByProductIdFk();

            if (userOfReviewDb.getUsername().equals("guest")) {
                guestReviews.add(reviewEntity);
            } else if (reviewEntityFactory.reviewExists(
                    databaseClient, userOfReviewDb, productOfReviewDb))
            {
                ReviewEntity reviewEntityDb = reviewEntityFactory.getReviewByUserAndProduct
                        (databaseClient,userOfReviewDb, productOfReviewDb);
                if(reviewEntityDb.getReviewDate().before(reviewEntity.getReviewDate())){
                    reviewEntityFactory.updateReviewEntity(reviewEntityDb, reviewEntity);
                    databaseClient.mergeEntity(reviewEntityDb);
                }

            } else {
                databaseClient.persistEntity(reviewEntity);
            }

        }
        databaseClient.persistEntities(guestReviews);
    }


}
