package Root.DataImport;

import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.*;
import Root.EntityFactory.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ShopDataImportController manages importing the datasets contained
 * in leipzig_transformed_nowhitespace.xml and dresden_nowhitespace.xml
 */
public class ShopDataImportController extends XmlDocumentImport {


    private final ShopEntityFactory shopEntityFactory = new ShopEntityFactory();
    private final Root.EntityFactory.OfferEntityFactory offerEntityFactory = new Root.EntityFactory.OfferEntityFactory();
    private final ProductEntityFactory productEntityFactory = new ProductEntityFactory();
    private final BookEntityFactory bookEntityFactory = new BookEntityFactory();
    private final PublisherEntityFactory publisherEntityFactory = new PublisherEntityFactory();
    private final DvdEntityFactory dvdEntityFactory = new DvdEntityFactory();
    private final LabelEntityFactory labelEntityFactory = new LabelEntityFactory();
    private final MusicCdEntityFactory musicCdEntityFactory = new MusicCdEntityFactory();
    private final SongEntityFactory songEntityFactory = new SongEntityFactory();
    private final SimilarProductsEntityFactory similarProductsEntityFactory = new SimilarProductsEntityFactory();
    private final MediaProductionRoleEntityFactory mediaProductionRoleEntityFactory = new MediaProductionRoleEntityFactory();
    private final MediaPersonEntityFactory mediaPersonEntityFactory = new MediaPersonEntityFactory();
    private final InvolvedInProductEntityFactory involvedInProductEntityFactory = new InvolvedInProductEntityFactory();
    List<String[]> mediaPersonRolesToImport = new ArrayList<>();


    public ShopDataImportController()
    {
        mediaPersonRolesToImport.add(new String[]{"actors", "actor"});
        mediaPersonRolesToImport.add(new String[]{"artists", "artist"});
        mediaPersonRolesToImport.add(new String[]{"authors", "author"});
        mediaPersonRolesToImport.add(new String[]{"creators", "creator"});
        mediaPersonRolesToImport.add(new String[]{"directors", "director"});
    }


    /**
     * Main method to control data import from leipzig_transformed.xml and dresden.xml
     */
    @Override
    public void importData(Document doc, DatabaseClient databaseClient)
    {

        // persist all the static media production roles

        for (String[] mediaProductionRole : mediaPersonRolesToImport) {
            if (!mediaProductionRoleEntityFactory.mediaProductionRoleExists(databaseClient, mediaProductionRole[1])) {
                databaseClient.persistEntity(mediaProductionRoleEntityFactory.generateFromName(mediaProductionRole[1]));
            }
        }

        // start shop import

        ShopEntity shopDb = importShop(doc, databaseClient);
        if (Objects.isNull(shopDb)) {
            return;
        }
        NodeList itemList = getItemNodeList(doc, "/shop/item");

        for (int i = 0; i < itemList.getLength(); i++) {
            Node currentItem = itemList.item(i);
            ProductEntity productDb = importProduct(currentItem, databaseClient, shopDb);
            if (Objects.isNull(productDb)) {
                continue;
            }

            importOffers(currentItem, shopDb, productDb, databaseClient);
        }
    }

    private List<SimilarProductsEntity> importSimilarProducts(Node currentItem, DatabaseClient databaseClient, ShopEntity shopDb, ProductEntity productDb)
    {

        List<SimilarProductsEntity> persistedSimilarProducts = new ArrayList<>();
        List<SimilarProductsEntity> similarProductsEntities = similarProductsEntityFactory
                .generateSimilarProductsEntitiesByNode(currentItem, shopDb, productDb, databaseClient);
        for (SimilarProductsEntity similarProductsEntity : similarProductsEntities) {
            if (databaseClient.persistEntity(similarProductsEntity)) {
                persistedSimilarProducts.add(similarProductsEntity);
            }
        }
        return persistedSimilarProducts;
    }

    private void importMediaPeople(Node currentItem, DatabaseClient databaseClient, List<String[]> mediaPersonRolesToImport, ShopEntity shopDb, ProductEntity productDb)
    {
        for (String[] mediaPersonRole : mediaPersonRolesToImport) {

            List<MediaPersonEntity> mediaPersonListDb = new ArrayList<MediaPersonEntity>();
            List<MediaPersonEntity> mediaPersonList =
                    mediaPersonEntityFactory.generateMediaPersonEntities
                            (currentItem, mediaPersonRole[0], mediaPersonRole[1], shopDb);
            // persist media role or get media role from db
            for (MediaPersonEntity mediaPersonEntity : mediaPersonList) {
                if (!mediaPersonEntityFactory.mediaPersonExists(databaseClient, mediaPersonEntity.getName())) {
                    databaseClient.persistEntity(mediaPersonEntity);
                }
                mediaPersonListDb.add(mediaPersonEntityFactory.getMediaPersonByName(databaseClient, mediaPersonEntity.getName()));
            }
            // generate and persist involed_in_product entities
            for (MediaPersonEntity mediaPersonEntityDb : mediaPersonListDb) {
                InvolvedInProductEntity involvedInProductEntity = involvedInProductEntityFactory.
                        generateInvolvedInProductEntity(
                                productDb,
                                mediaPersonEntityDb,
                                mediaProductionRoleEntityFactory.
                                        getMediaProductionRoleByName(databaseClient, mediaPersonRole[1])
                        );
                if (!involvedInProductEntityFactory.involvedInProductExists
                        (
                                databaseClient,
                                productDb.getProductId(),
                                mediaPersonEntityDb.getMediaPersonId(),
                                mediaProductionRoleEntityFactory.getMediaProductionRoleByName
                                        (databaseClient, mediaPersonRole[1]).getMediaProductionRoleId())) {
                    databaseClient.persistEntity(involvedInProductEntity);
                }
            }
        }
    }

    /**
     * Import the shop of the document.
     * Lookup if shop already exists
     * - If true return the existing shopEntity
     * - If false try to persist the generated shopEntity
     * -- If successful return the newly persisted shopEntity
     * -- If unsuccessful return null
     *
     * @param doc
     * @param databaseClient
     * @return
     */
    private ShopEntity importShop(Document doc, DatabaseClient databaseClient)
    {
        ShopEntity shop = shopEntityFactory.generateShopEntityByDocument(doc);
        if (!shopEntityFactory.shopExists(databaseClient, shop.getShopName(), shop.getStreet(), shop.getZip())) {
            if (databaseClient.persistEntity(shop)) {
                return shop;
            } else {
                return null;
            }
        } else {
            return shopEntityFactory.getShopEntityByNameStreetZip
                    (databaseClient, shop.getShopName(), shop.getStreet(), shop.getZip());
        }
    }

    /**
     * Import the product corresponding to the current node
     * 1. Lookup if product exists
     * - If true return the existing productEntity
     * - Else try to persist the generated productEntity
     * --If successful
     * ---persist additional information book|dvd|musicCd
     * ---return product
     * --Else return null
     *
     * @param currentItem
     * @param databaseClient
     * @param shopDb
     * @return
     */
    private ProductEntity importProduct(Node currentItem, DatabaseClient databaseClient, ShopEntity shopDb)
    {
        ProductEntity productEntity = productEntityFactory
                .generateProductEntityByNode(currentItem, shopDb.getShopName());
        if (productEntityFactory.productExists(databaseClient, productEntity.getAsin())) {
            return productEntityFactory.getProductByAsin(databaseClient, productEntity.getAsin());
        } else if (databaseClient.persistEntity(productEntity)) {
            boolean successful = false;
            switch (productEntity.getProductGroup()) {
                case "Book":
                    successful = importBookInformation(currentItem, databaseClient, shopDb, productEntity);
                    break;
                case "DVD":
                    successful = importDVDInformation(currentItem, databaseClient, productEntity);
                    break;
                case "Music":
                    successful = importMusicCdInformation(currentItem, databaseClient, shopDb, productEntity);
                    break;
            }
            if (!successful) {
                databaseClient.removeEntity(productEntity);
                return null;
            }
            importMediaPeople(currentItem, databaseClient, mediaPersonRolesToImport, shopDb, productEntity);
            importSimilarProducts(currentItem, databaseClient, shopDb, productEntity);
            return productEntity;
        } else {
            return null;
        }
    }

    private boolean importBookInformation(Node currentItem, DatabaseClient databaseClient, ShopEntity shopDb, ProductEntity productDb)
    {
        // generate the bookEntityFactory from node
        BookEntity bookEntity = bookEntityFactory.
                generateBookEntityByNode(currentItem, productDb, null);

        // generate the labels from node and check if they already exists in the database
        List<PublisherEntity> publisherEntityListFromNodes = publisherEntityFactory
                .generatePublisherEntitiesByNode(currentItem, shopDb.getShopName(), bookEntity);

        List<PublisherEntity> publisherEntityList = new ArrayList<PublisherEntity>();

        List<Object> notPersistedObjectsList = new ArrayList<Object>();

        for (int i = 0; i < publisherEntityListFromNodes.size(); i++) {
            PublisherEntity publisherEntity = publisherEntityListFromNodes.get(i);
            if (!publisherEntityFactory.publisherExists(databaseClient, publisherEntity.getName())) {
                // if the publisher is not in the database yet
                publisherEntityList.add(publisherEntity);
                notPersistedObjectsList.add(publisherEntity);
            } else {
                // if the label is already in the database
                publisherEntityList.add(publisherEntityFactory
                        .getPublisherByName(databaseClient, publisherEntity.getName()));
            }
        }

        bookEntity.setPublisherEntityList(publisherEntityList);
        //persist book
        boolean success = databaseClient.persistEntity(bookEntity);
        //persist miscellaneous information
        if (success) {
            databaseClient.persistEntities(notPersistedObjectsList);
        }
        return success;
    }

    private boolean importDVDInformation(Node currentItem, DatabaseClient databaseClient, ProductEntity productDb)
    {
        // insert the book into the database
        DvdEntity dvdEntity = dvdEntityFactory.generateDvdEntityByNode(currentItem, productDb);
        return databaseClient.persistEntity(dvdEntity);
    }

    private boolean importMusicCdInformation(Node currentItem, DatabaseClient databaseClient, ShopEntity shopDb, ProductEntity productDb)
    {
        // We should have in mind:
        // "It's a common mistake to only set one side of the relationship,
        // persist the entity, and then observe that the join table is empty.
        // Setting both sides of the relationship will fix this."
        // https://www.infoworld.com/article/3387643/java-persistence-with-jpa-and-hibernate-part-2-many-to-many-relationships.html


        // generate the musicCdEntity from node
        MusicCdEntity musicCdEntity = musicCdEntityFactory.
                generateMusicCdEntityByNode(currentItem, productDb, null);

        // generate the labels from node and check if they already exists in the database
        List<LabelEntity> labelEntityListFromNodes = labelEntityFactory
                .generateLabelsEntitiesByNode(currentItem, shopDb.getShopName(), musicCdEntity);

        List<LabelEntity> labelEntityList = new ArrayList<LabelEntity>();

        List<Object> notPersistedObjectsList = new ArrayList<Object>();
        notPersistedObjectsList.add(musicCdEntity);

        for (int i = 0; i < labelEntityListFromNodes.size(); i++) {
            LabelEntity labelEntity = labelEntityListFromNodes.get(i);
            if (!labelEntityFactory.labelExists(databaseClient, labelEntity.getName())) {
                // if the label is not in the database yet
                labelEntityList.add(labelEntity);
                notPersistedObjectsList.add(labelEntity);
            } else {
                // if the label is already in the database
                labelEntityList.add(labelEntityFactory
                        .getLabelByName(databaseClient, labelEntity.getName()));
            }

        }

        // update musicCdEntity with the
        musicCdEntity.setLabelEntityList(labelEntityList);


        // insert songs into the database
        List<SongEntity> songEntityList = songEntityFactory.generateSongEntitiesByNode(currentItem, musicCdEntity);
        notPersistedObjectsList.addAll(songEntityList);

        boolean success = databaseClient.persistEntity(musicCdEntity);
        if (success) {
            databaseClient.persistEntities(notPersistedObjectsList);
        }
        return success;
    }

    /**
     * Imports all offerEntities corresponding to a given product node
     * 1. Generate all offerEntities of the node
     * 2. Try to persist each of them
     * 3. return all successfully persisted offerEntities
     *
     * @param currentItem
     * @param shopDb
     * @param productDb
     * @param databaseClient
     * @return
     */
    private List<OfferEntity> importOffers(Node currentItem, ShopEntity shopDb, ProductEntity productDb, DatabaseClient databaseClient)
    {
        List<OfferEntity> persistedOffers = new ArrayList<>();
        List<OfferEntity> offerEntities = offerEntityFactory
                .generateOfferEntitiesByNode(currentItem, shopDb, productDb);
        for (OfferEntity offerEntity : offerEntities) {
            if (databaseClient.persistEntity(offerEntity)) {
                persistedOffers.add(offerEntity);
            }
        }
        return persistedOffers;
    }


}
