package Root.DataImport;


import Root.DatabaseOperations.DatabaseClient;
import Root.Entities.CategoryEntity;
import Root.Entities.CategoryEntryEntity;
import Root.EntityFactory.CategoryEntityFactory;
import Root.EntityFactory.CategoryEntryEntityFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Objects;

public class CategoryImportController extends XmlDocumentImport {

    private final CategoryEntityFactory categoryEntityFactory = new CategoryEntityFactory();
    private final CategoryEntryEntityFactory categoryEntryEntityFactory = new CategoryEntryEntityFactory();


    public void importData(Document doc, DatabaseClient databaseClient)
    {
        String xPath = "/categories/category";
        NodeList itemList = getItemNodeList(doc, xPath);
        for (int i = 0; i < itemList.getLength(); i++) {
            Node currentItem = itemList.item(i);
            CategoryEntity mainCategory = categoryEntityFactory.generateMainCategoryByNode(currentItem);
            databaseClient.persistEntity(mainCategory);
            cascadeSubCategoriesAsBatch(mainCategory, currentItem, databaseClient);
        }
    }


    private void cascadeSubCategoriesAsBatch(CategoryEntity mainCategoryEntity, Node mainCategoryNode, DatabaseClient databaseClient)
    {
        NodeList subCategoryList = mainCategoryNode.getChildNodes();
        for (int i = 0; i < subCategoryList.getLength(); i++) {
            Node subNode = subCategoryList.item(i);
            if(!(subNode.getNodeType() == Node.ELEMENT_NODE)){
                continue;
            }
            if (subNode.getNodeName().equals("item")) {
                CategoryEntryEntity categoryEntryEntity = categoryEntryEntityFactory.generateCategoryEntryEntityByNode(
                        databaseClient, subNode, mainCategoryEntity);
                if(Objects.nonNull(categoryEntryEntity.getProductByProductIdFk())){
                    databaseClient.persistEntity(categoryEntryEntity);
                }
            } else if (subNode.getNodeName().equals("category")) {
                CategoryEntity subCategoryEntity = categoryEntityFactory.generateSubCategoryByNode(subNode, mainCategoryEntity);
                if(databaseClient.persistEntity(subCategoryEntity)){
                    cascadeSubCategoriesAsBatch(subCategoryEntity, subNode, databaseClient);
                }
            }
        }
    }
}





