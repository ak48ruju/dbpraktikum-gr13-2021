package Root.DataImport;

import Root.DatabaseOperations.DatabaseClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public abstract class XmlDocumentImport {
    private final XPath xPath = XPathFactory.newInstance().newXPath();

    public abstract void importData(Document doc, DatabaseClient databaseClient);

    /**
     * Collects all  items contained in the document
     *
     * @param doc xml document
     * @param xPathString xPath to collect nodes from
     * @return NodeList containing all shop items | exit otherwise
     */
    protected NodeList getItemNodeList(Document doc, String xPathString)
    {
        try {
            return (NodeList) xPath.compile(xPathString).evaluate(doc, XPathConstants.NODESET);

        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        System.out.println("XPATH: Could not get create NodeList for ");
        System.exit(-1);
        return null;
    }
}
